<?php
/**
 My model base class
 version 1.0.0;
 * **/
class baseModel
{
    var $data;
    var $tbl;
    var $dbname;
	var $con;
    var $result;
	public function findByAttribute($distinc=null,$filter=null){
        $db="{$this->dbname}.dbo.{$this->tbl}";
        $distinc = ($distinc==null)? "*" : $distinc;
        $filter=($filter==null)? "" : "WHERE {$filter}";
		$sql="SELECT $distinc FROM $db $filter";
		return $this->fetchArray($sql);
    }
    public function updateByAttribute($colname,$filter){
        $db="{$this->dbname}.dbo.{$this->tbl}";
        $sql="UPDATE $db set $colname WHERE $filter";
        self::query($sql);
    }
    public function save($colname,$values){
        $db="{$this->dbname}.dbo.{$this->tbl}";
		$sql = "INSERT INTO $db ($colname) values($values)";
		self::query($sql);
    }
    //NOTHING TO EDIT BELOW///
    public function __construct ($db,$tbl=null,$dbname=null){
		$this->dbname=$dbname;$this->tbl=$tbl; 
		$db1="RanUser";
		$host = $db['host'];
		$this->con = odbc_connect("Driver={$db['driver']};Server=$host;Database=$db1;", $db['user'], $db['pass']);
    }
	protected function query ($query){
		//echo $query;
		$this->result= odbc_exec($this->con,$query); 
    }
	public function fetchArray($sql){
		self::query($sql);
		$data=array();
		while( $row = odbc_fetch_array($this->result) ) { 
			$data[]=$row;
		} 
		return $data;
	}
	function __destruct (){
		odbc_close($this->con);
		$this->result="";
    }
}
