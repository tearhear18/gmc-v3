<?php
class filter{
	
	//filter level//
	public static function level1($a){
		// $data = preg_replace('/[^-.@a-zA-Z0-9_]/s', '', $a); //good filter
		$data = preg_replace('/[^-.+\/@=a-zA-Z0-9_\\\\]/s', '', $a);
		//$data = preg_replace('#[^-_a-zA-Z0-9-._@=\/]#i', '', $a);
		//$regEx="/[^a-zA-Z0-9 !@==._]/"; 
		//$data = preg_replace($regEx,"",$a);
		
		return $data;
	}
	//filter level2//
	public static function level2($a){
		
		$data = preg_replace('/[^-.@a-zA-Z0-9_]/s', '', $a); //allow only letters -_@./
		
		return $data;
	}
	//filter level 3//
	public static function level3($a){
		
		$data=preg_replace("/[^0-9]/", "",$a); //numbersonly
		
		return $data;
	}
	
}