<?php
define("delimiter", ",");
//define("FILENAME", "../item.csv");
define("FILENAME",dirname(__FILE__)."/item.csv");
class item_csv{
	var $item_offset;
	var $emItemType=array("Weapon Tool",
			"Arrow",
			"Pharmaceutical",
			"Skill Scroll",
			"Revert's Book",
			"Authentication Book",
			"Enhancer",
			"Magic Incantations",
			"Bus Ticket",
			"Oblivion Potion (Not Used)",
			"Oblivion Potion (Not Used)",
			"Oblivion Potion",
			"Gift Box",
			"Forgetful Detergent",
			"Megaphone",
			"Firecracker",
			"Character Add Card",
			"Inventory Add Card",
			"Locker Add Card",
			"Locker Connection Card",
			"Premium Box",
			"Store Permit Card",
			"Random Box",
			"Costume Detergent",
			"Hair Style Card",
			"Face Transform Card",
			"? Item",
			"Authentication CD",
			"Friend Connection Card",
			"Summon Cluber",
			"Hair Shop Ticket",
			"Name Change Card",
			"Hair Style Card",
			"Hair Color Card",
			"Resurrection Rosary",
			"Pet Card",
			"Pet Food",
			"Pet Name Change Card",
			"Pet Color Change Card",
			"Pet Style Change Card",
			"Pet Skill Scroll",
			"SMS Send",
			"PET Resurrection Card",
			"Safe Guard Rosary",
			"Reform Card",
			"S.R.C",
			"S.R.C Fuel",
			"Item Get Card",
			"Exp Get Card",
			"Sex Change Card",
			"Garbage Card",
			"Map Pass",
			"PET Shape Change",
			"Face Change Card",
			"Taxi Card",
			"Material Item",
			"NPC Call card",
			"Neutron Bullet",
			"Lunchbox",
			"Pet Dual Skill Card",
			"Reinforcement Rosary",
			"Mitigation Rosary",
			"Bike Color Change Card",
			"Bike Boost Card",
			"New Oblivion Potion",
			"Costume Color Change Card",
			"Mailbox Card",
			"Point Card(Refundable)",
			"point card(No Refundable)",
			"Item Mix Scroll",
			"Item Summon Card",
			"Item Slot Material",
			"Item Consume");
	var $emLevel=array("Common Item",
			"Rare Item",
			"Unique Item",
			"Vote Point Item",
			"Premium Item");
	var $emSuit=array("Hats",
			"Top",
			"Pants",
			"Glove",
			"Shoes",
			"Arms",
			"Necklace",
			"Bracelet",
			"Ring",
			"Pet Costume A",
			"Pet Costume B",
			"S.R.C",
			"S.R.C Skin",
			"S.R.C Parts A",
			"S.R.C Parts B",
			"S.R.C Parts C",
			"S.R.C Parts D",
			"S.R.C Parts E",
			"S.R.C Parts F",
			"Wing",
			"Aura",
			"Belt",
			"Earring",
			"Accessory",
			"Consumable");
		var $emDrug=array("NONE",
			"HP Recovery",
			"MP Recovery",
			"SP Recovery",
			"HP+MP ReCovery",
			"HP+SP Recovery",
			"HP+MP+SP Recovery",
			"Extraodinary Treatment",
			"Back to School",
			"Back to Starting Point",
			"Back to Before",
			"Revive function",
			"HP Recovery+Extraodinary Treatment",
			"HP+MP+SP Recovery+Extraodinary Treatment",
			"Move to Specific Location",
			"CP Recovery");
	var 	$CSV_BUFFER_ADDRESS=array();
	public function read(){
		
				$MAIN_STREAM=file(FILENAME);  //this will unpack the memory//;
				$FILE_HEADER = self::ExtractMemAddress($MAIN_STREAM[0]);
				
				
				$CSV_SIZE=self::getHeaderSize($MAIN_STREAM);
				

				for($a=1;$a<$CSV_SIZE;$a++){
					$data=self::ExtractMemAddress($MAIN_STREAM[$a]);
					$this->CSV_BUFFER_ADDRESS[$data[4]]["strName"]=preg_replace("/[^A-Za-z0-9?!\s]/","",$data[5]);
					$this->CSV_BUFFER_ADDRESS[$data[4]]["emItemType"]=$this->emItemType[$data[18]];
					$this->CSV_BUFFER_ADDRESS[$data[4]]["emSuit"]=$this->emSuit[$data[76]];
					$this->CSV_BUFFER_ADDRESS[$data[4]]["emLevel"]=$this->emLevel[$data[6]];
					$this->CSV_BUFFER_ADDRESS[$data[4]]["emDrug"]=$this->emDrug[$data[118]];
					$this->CSV_BUFFER_ADDRESS[$data[4]]["mid_sid"]=$data[4];
					
					
				}
				$this->item_offset = $this->CSV_BUFFER_ADDRESS;
		
	}
	
	
	public function getHeaderSize($memory){
		$LAST_KEY = key( array_slice( $memory, -1, 1, TRUE ) );
		return $LAST_KEY;
	}
	public function ExtractMemAddress($ADDR){
		return explode(delimiter,$ADDR);
	}
		
}


