<?php
//error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class update_item_inven extends controller{
    private $ChaNum;
	private $memory;
	
	private $pdmg;
	private $pdf;
	private $pqty;
	
	private $pfire;
	private $pice;
	private $pelec;
	private $ppoi;
	private $pwind;
	
	private $opt1;
	private $opt2;
	private $opt3;
	private $opt4;
	private $val1;
	private $val2;
	private $val3;
	private $val4;
	
	private $_chainven;
	private $_buffer_memory;
    private $requirement;
	
	
    function __construct($requirement,$data=null) {
			
            $this->ChaNum=$data['ChaNum'];
			$this->memory=$data['nofilter']['memory'];
			
			//$this->pX		=self::trimoffset($data['pX']); //position X
			//$this->pY		=self::trimoffset($data['pY']);
			//$this->pmid		=$data['pmid'];
			//$this->psid		=$data['psid'];
			$this->pdmg		=self::trimoffset($data['pdmg']);
			$this->pdf		=self::trimoffset($data['pdf']);
			$this->pqty		=self::trimOpt($data['pqty']);
			$this->pfire	=self::trimoffset($data['pfire']);
			$this->pice		=self::trimoffset($data['pice']);
			$this->pelec	=self::trimoffset($data['pelec']);
			$this->ppoi		=self::trimoffset($data['ppoi']);
			$this->pwind	=self::trimoffset($data['pwind']);
			$this->opt1		=self::trimoffset($data['opt1']);
			$this->opt2		=self::trimoffset($data['opt2']);
			$this->opt3		=self::trimoffset($data['opt3']);
			$this->opt4		=self::trimoffset($data['opt4']);
			$this->val1		=self::trimOpt($data['val1']);
			$this->val2		=self::trimOpt($data['val2']);
			$this->val3		=self::trimOpt($data['val3']);
			$this->val4		=self::trimOpt($data['val4']);
			
            $this->requirement=$requirement;
			
			self::fetchInventory();
			
			if(self::Process_UpdateItem()){
				self::SaveMode();
				self::errorCode(2);
			}else{
				self::errorCode(1);
			}
			
			
    }
    private function fetchInventory(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaInven";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_chainven=bin2hex($result[0]['ChaInven']);
			
            }
           
    }
	
	private function Process_UpdateItem(){
		
		$CHA_INVEN=$this->_chainven;
		$this->_chainven=null;  //destroy old mem//
		
		$INVEN_HEAD = substr($CHA_INVEN,0,24);   
		$INVEN_SIZE = hexdec(self::getMemory($INVEN_HEAD,16,2));         	//count item in inventory//
		$FILE_SIZE = strlen($CHA_INVEN);					//count the total hex digits
		$ITEM = substr($CHA_INVEN,24,$FILE_SIZE);
		$MEM_SIZE=(self::getData($INVEN_HEAD,0,2)==20)? 176 :160; //sub versioning//
		$ITEM_LIST = str_split($ITEM,$MEM_SIZE);
		
		
		if (in_array($this->memory,$ITEM_LIST)) {   //checking memory existence in buffer 
			$temp_arr = array_flip($ITEM_LIST);  	//reverse data ket to value
			$indexOf = $temp_arr[$this->memory];
			//re-packing method//
			
			$ITEM_GRADE			=$this->pdmg.$this->pdf;
			$ITEM_ELEMENT		=$this->pfire.$this->pice.$this->pelec.$this->ppoi.$this->pwind;
			$ITEM_OPTIONnVALUE	=$this->opt1.$this->opt3.$this->opt2.$this->opt4.$this->val1.$this->val3.$this->val2.$this->val4;
			
			$item_buffer_M = $this->memory;
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_GRADE,90,4);
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_ELEMENT,94,10);
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_OPTIONnVALUE,104,24);
			$item_buffer_M = self::updateoffset($item_buffer_M,$this->pqty,80,4);
			
			$ITEM_LIST[$indexOf]=$item_buffer_M;
			$this->_buffer_memory=$INVEN_HEAD.implode($ITEM_LIST);
			return true;
		}else{
			
			return false;
		}
		
		
	}
	private function SaveMode(){
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaInven=convert(varbinary(MAX),0x$this->_buffer_memory)",$filter);
       
	}
	
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getData($a,$b,$c){return hexdec(substr($a,$b,$c));}
	public function getOffset($a,$b,$c){$f=substr($a,$b,2)."".substr($a,$c,2);return hexdec($f);}
	public function ENUM($a,$b,$c){return hexdec(substr($a,$b,$c));}
	public function trimID($a,$b,$c){$b=substr($a,$b,$c);$c=str_split($b,2);$d=$c[1].$c[0];return hexdec($d);}
	public function fixID($a){$b=dechex($a);$c=str_pad(strtoupper($b),4,0,STR_PAD_LEFT);$d=str_split($c,2);return $d[1].$d[0];}
	public function trimoffset($a){$b=dechex($a);return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);}
	public function trimOpt($a){$b=dechex($a);$c=str_pad(strtoupper($b),4,0,STR_PAD_LEFT);$d=str_split($c,2);return $d[1].$d[0];}
	public function updateoffset($a,$b,$c,$d){return substr_replace($a,$b,$c,$d);}
	
}