<?php
error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class Delete_SuitItem extends controller{
    private $ChaNum;
	private $memory;
	private $_ChaPutOnItems;
	private $_buffer_memory;
    private $requirement;
    function __construct($requirement,$data=null) {
			
            $this->ChaNum=$data['ChaNum'];
			$this->memory=$data['nofilter']['memory'];
            $this->requirement=$requirement;
			
			self::fetchInventory();
			if(self::Process_DeleteItem()){
				self::SaveMode();
				self::errorCode(2);
			}else{
				self::errorCode(1);
			}
			
			
    }
    private function fetchInventory(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaPutOnItems";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_ChaPutOnItems=bin2hex($result[0]['ChaPutOnItems']);
			
            }
           
    }
	
	private function Process_DeleteItem(){
		$inven_memory=$this->_ChaPutOnItems;
		$item_suit_header=self::getMemory($inven_memory,0,24);
		$item_suit_size=strlen($inven_memory);
		$item_suit_size-=1;
		$item_suit_mem = self::getMemory($inven_memory,24,3384);
		
		$MEM_SIZE=(self::getData($item_suit_header,0,2)==20)? 160 :144;  //sub versioning 
		if($MEM_SIZE==160){
			$EMPTY_MEMORY="ffffffffffffffff0000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000c010000000000ff7f177ca8091d00";
		}else{
			$EMPTY_MEMORY="ffffffffffffffff0000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000c010000000000";
		}
		$ITEM_LIST=str_split($item_suit_mem,$MEM_SIZE);
		
		
		if (in_array($this->memory,$ITEM_LIST)) {   //checking memory existence in buffer 
			$ITEM_LISTz = array_flip($ITEM_LIST);  	//reverse data ket to value
			$n_Pointer=$ITEM_LISTz[$this->memory];
			
			$ITEM_LIST[$n_Pointer]=$EMPTY_MEMORY;
		
			
			$this->_buffer_memory = $item_suit_header.implode($ITEM_LIST);
			
			$ITEM_LIST=null;
			return true;
		}else{
			
			return false;
		}
		
		
	}
	private function SaveMode(){
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaPutOnItems=convert(varbinary(MAX),0x$this->_buffer_memory)",$filter);
       
	}
	public function trimID($a,$b,$c){
		$b=substr($a,$b,$c);
		$c=str_split($b,2);
		$d=$c[1].$c[0];
		return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}
	public function digitz($a){
		$b=dechex($a);
		return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);
	}

	function updateoffset($a,$b,$c,$d){
		return substr_replace($a,$b,$c,$d);
	}
	
}