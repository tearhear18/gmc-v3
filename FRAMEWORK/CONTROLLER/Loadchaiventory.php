<?php
error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class Loadchaiventory extends controller{
    private $ChaNum;
	private $_chainven;
	private $_chamoney;
    private $requirement;
    function __construct($requirement,$data=null) {
		
            $this->ChaNum=$data['ChaNum'];
            $this->requirement=$requirement;
			
			self::fetchInventory();
            self::DecodeInventory();
          
    }
    private function fetchInventory(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaInven,ChaMoney";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_chainven=bin2hex($result[0]['ChaInven']);
				$this->_chamoney=$result[0]['ChaMoney'];
            }
           
    }
	
	private function DecodeInventory(){
		$csv = new item_csv;
		$csv->read();  //fireup csv and check for flash cache version :D 
		
		$item=array();
		
		$CHA_INVEN=$this->_chainven;
		
		
		$this->_chainven=null;  //destroy old mem//
		
		$INVEN_HEAD = substr($CHA_INVEN,0,24);    //inventory header//
		$INVEN_SIZE = hexdec(self::getMemory($INVEN_HEAD,16,2));         	//count item in inventory//
		$FILE_SIZE = strlen($CHA_INVEN);					//count the total hex digits
		$ITEM = substr($CHA_INVEN,24,$FILE_SIZE);
		$MEM_SIZE=(self::getData($INVEN_HEAD,0,2)==20)? 176 :160;
		$ITEM_LIST = str_split($ITEM,$MEM_SIZE);
		if($INVEN_SIZE!=0){
		foreach($ITEM_LIST as $em_Buffer){
			$item['tab1']['MEM'][]=$em_Buffer;
			$MID = str_pad(self::trimID($em_Buffer,16,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,20,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			$item['tab1']['pos'][]="x".self::getData($em_Buffer,0,2)."y".self::getData($em_Buffer,4,2);
			$item['tab1']['X'][]=self::getData($em_Buffer,0,2);
			$item['tab1']['Y'][]=self::getData($em_Buffer,4,2);
			$item['tab1']['MID'][]=$MID;
			$item['tab1']['SID'][]=$SID;
			$item['tab1']['QTY'][]=self::getOffset($em_Buffer,82,80);
			$item['tab1']['ItemName'][]=$csv->item_offset[$address]["strName"];
			$item['tab1']['emItemType'][]=$csv->item_offset[$address]["emItemType"];
			$item['tab1']['emDrug'][]=$csv->item_offset[$address]["emDrug"];
			$item['tab1']['emLevel'][]=$csv->item_offset[$address]["emLevel"];
			$item['tab1']['DMG_GRADE'][]=self::getData($em_Buffer,90,2);//+damage upgrade///
			$item['tab1']['DEF_GRADE'][]=self::getData($em_Buffer,92,2); //+defense upgrade//
			$item['tab1']['ELEC'][]	=self::getData($em_Buffer,98,2); //+elec upgrade//
			$item['tab1']['FIRE'][]	=self::getData($em_Buffer,94,2); //+fire upgrade//
			$item['tab1']['ICE'][]		=self::getData($em_Buffer,96,2); //+ice upgrade//
			$item['tab1']['POISON'][]	=self::getData($em_Buffer,100,2); //+poison upgrade//
			$item['tab1']['WIND'][]	=self::getData($em_Buffer,102,2); //+wind upgrade//
			$item['tab1']['Opt1'][]	=self::getData($em_Buffer,104,2);
			$item['tab1']['Opt2'][]	=self::getData($em_Buffer,108,2);
			$item['tab1']['Opt3'][]	=self::getData($em_Buffer,106,2);
			$item['tab1']['Opt4'][]	=self::getData($em_Buffer,110,2);
			$item['tab1']['Val1'][]	=self::getOffset($em_Buffer,114,112);
			$item['tab1']['Val2'][]	=self::getOffset($em_Buffer,122,120);
			$item['tab1']['Val3'][]	=self::getOffset($em_Buffer,118,116);
			$item['tab1']['Val4'][]	=self::getOffset($em_Buffer,126,124);
			
		}
		}else{ 
				$item['tab1']['pos']="none";
		}
		$item['gold']=number_format($this->_chamoney);
		self::pushJSON($item); 
	}
	public function trimID($a,$b,$c){
		$b=substr($a,$b,$c);
		$c=str_split($b,2);
		$d=$c[1].$c[0];
		return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}



	
}