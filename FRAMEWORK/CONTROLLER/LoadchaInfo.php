<?php
class LoadchaInfo extends controller{
    private $ChaNum;
	private $data=array();
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->ChaNum=$data['ChaNum'];
            $this->requirement=$requirement;
			self::lookup();
            
    }
    private function lookup(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaNum,UserNum,ChaName,ChaGuName,ChaClass,ChaSchool,ChaPower,ChaStrong,ChaStrength,ChaSpirit,ChaAgility,ChaDex,ChaStRemain,ChaMoney,ChaPremiumPoint,ChaVotePoint,ChaLevel,ChaReborn,ChaSkillPoint";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
            }else{
				$data=array();
				$this->data=$result;
				$this->data[0]['ChaMoney']=number_format($result[0]['ChaMoney']);
				$this->data[0]['ChaVotePoint']=number_format($result[0]['ChaVotePoint']);
				$this->data[0]['ChaPremiumPoint']=number_format($result[0]['ChaPremiumPoint']);
				self::pushJSON($this->data); 
                
            }
           
    }
	
	
}