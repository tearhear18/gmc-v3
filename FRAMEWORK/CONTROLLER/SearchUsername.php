<?php
class SearchUsername extends controller{
    private $UserName;
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->UserName=$data['UserName'];
            $this->requirement=$requirement;
			self::lookup();
            
    }
    private function lookup(){
          
            $model= $this->requireModel('UserInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="UserID,UserNum,UserType,UserLoginState";
    
            //add filters//
            $filter="UserName like '%$this->UserName%'";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
            }else{
               
                self::pushJSON($result); 
                
            }
           
    }
	
}