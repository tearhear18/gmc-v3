<?php
class Searchcharacter extends controller{
    private $ChaName;
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->ChaName=$data['ChaName'];
            $this->requirement=$requirement;
			self::lookup();
            
    }
    private function lookup(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaNum,ChaName,ChaOnline,ChaDeleted";
    
            //add filters//
            $filter="ChaName like '%$this->ChaName%'";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
            }else{
               
                self::pushJSON($result); 
                
            }
           
    }
}