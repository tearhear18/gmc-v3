<?php
error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class LoadUserLocker extends controller{
    private $UserNum;
	private $_userinven;
	private $_usermoney;
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->UserNum=$data['UserNum'];
            $this->requirement=$requirement;
			self::fetchLocker();
            self::DecodeLocker();
          
    }
    private function fetchLocker(){
          
            $model= $this->requireModel('UserInven',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="UserInven,UserMoney";
    
            //add filters//
            $filter="UserNum=$this->UserNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_userinven=bin2hex($result[0]['UserInven']);
				$this->_usermoney=$result[0]['UserMoney'];
            }
           
    }
	
	private function DecodeLocker(){
		$csv = new item_csv;
		$csv->read();
		
		$locker_chunk=array();
		$item=array();
		$mem_location=0;
		$inven_memory=$this->_userinven;
		$this->_userinven=null;
		//////////////////////////////locker1
		$locker_version=self::getMemory($inven_memory,0,8);
		$locker_1_head=self::getMemory($inven_memory,8,24);
		$MEM_SIZE=(self::getData($locker_1_head,0,2)==20)? 176 :160;
		$locker_1_size=hexdec(self::getMemory($locker_1_head,16,2));
		$locker_1_length=$MEM_SIZE*$locker_1_size;
		if($locker_1_size!=0){
			$locker_1_memory=self::getMemory($inven_memory,32,$locker_1_length);
			$locker_chunk[1]=str_split($locker_1_memory,$MEM_SIZE);
			
		}
		/////////////////////////////locker2
		$mem_location=32+$locker_1_length;
		$locker_2_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_2_size=hexdec(self::getMemory($locker_2_head,16,2));
		$locker_2_length=$MEM_SIZE*$locker_2_size;
		if($locker_2_size!=0){
			
			$locker_2_memory=self::getMemory($inven_memory,$mem_location+24,$locker_2_length);
			$locker_chunk[2]=str_split($locker_2_memory,$MEM_SIZE);
			
		}
		/////////////////////////////locker3
		$mem_location=32+$locker_1_length+24+$locker_2_length;
		$locker_3_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_3_size=hexdec(self::getMemory($locker_3_head,16,2));
		$locker_3_length=$MEM_SIZE*$locker_3_size;
		if($locker_3_size!=0){
			$locker_3_memory=self::getMemory($inven_memory,$mem_location+24,$locker_3_length);
			$locker_chunk[3]=str_split($locker_3_memory,$MEM_SIZE);
		}
		//////////////////////////////locker4
		$mem_location=32+$locker_1_length+24+$locker_2_length+24+$locker_3_length;
		$locker_4_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_4_size=hexdec(self::getMemory($locker_4_head,16,2));
		$locker_4_length=$MEM_SIZE*$locker_4_size;
		if($locker_4_size!=0){
			$locker_4_memory=self::getMemory($inven_memory,$mem_location+24,$locker_4_length);
			$locker_chunk[4]=str_split($locker_4_memory,$MEM_SIZE);
		}
		//////////////////////////////locker5
		$mem_location=32+$locker_1_length+24+$locker_2_length+24+$locker_3_length+24+$locker_4_length;
		$locker_5_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_5_size=hexdec(self::getMemory($locker_5_head,16,2));
		$locker_5_length=$MEM_SIZE*$locker_5_size;
		if($locker_5_size!=0){
			$locker_5_memory=self::getMemory($inven_memory,$mem_location+24,$locker_5_length);
			$locker_chunk[5]=str_split($locker_5_memory,$MEM_SIZE);
		}
		
		if($locker_1_size!=0){
		foreach($locker_chunk[1] as $em_Buffer){
			$MID = str_pad(self::trimID($em_Buffer,16,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,20,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			$item['tab1']['pos'][]="x".self::getData($em_Buffer,0,2)."y".self::getData($em_Buffer,4,2);
			$item['tab1']['X'][]=self::getData($em_Buffer,0,2);
			$item['tab1']['Y'][]=self::getData($em_Buffer,4,2);
			$item['tab1']['MID'][]=$MID;
			$item['tab1']['SID'][]=$SID;
			$item['tab1']['QTY'][]=self::getOffset($em_Buffer,82,80);
			$item['tab1']['MEM'][]=$em_Buffer;
			$item['tab1']['ItemName'][]=$csv->item_offset[$address]["strName"];
			$item['tab1']['emItemType'][]=$csv->item_offset[$address]["emItemType"];
			$item['tab1']['emDrug'][]=$csv->item_offset[$address]["emDrug"];
			$item['tab1']['emLevel'][]=$csv->item_offset[$address]["emLevel"];
			$item['tab1']['DEF_GRADE'][]=self::getData($em_Buffer,90,2);//+damage upgrade///
			$item['tab1']['DMG_GRADE'][]=self::getData($em_Buffer,92,2); //+defense upgrade//
			$item['tab1']['ELEC'][]	=self::getData($em_Buffer,98,2); //+elec upgrade//
			$item['tab1']['FIRE'][]	=self::getData($em_Buffer,94,2); //+fire upgrade//
			$item['tab1']['ICE'][]		=self::getData($em_Buffer,96,2); //+ice upgrade//
			$item['tab1']['POISON'][]	=self::getData($em_Buffer,100,2); //+poison upgrade//
			$item['tab1']['WIND'][]	=self::getData($em_Buffer,102,2); //+wind upgrade//
			$item['tab1']['Opt1'][]	=self::getData($em_Buffer,104,2);
			$item['tab1']['Opt2'][]	=self::getData($em_Buffer,108,2);
			$item['tab1']['Opt3'][]	=self::getData($em_Buffer,106,2);
			$item['tab1']['Opt4'][]	=self::getData($em_Buffer,110,2);
			$item['tab1']['Val1'][]	=self::getOffset($em_Buffer,114,112);
			$item['tab1']['Val2'][]	=self::getOffset($em_Buffer,122,120);
			$item['tab1']['Val3'][]	=self::getOffset($em_Buffer,118,116);
			$item['tab1']['Val4'][]	=self::getOffset($em_Buffer,126,124);
			
		}
		}else{ 
				$item['tab1']['pos']="none";
		}
		if($locker_2_size!=0){
		
		foreach($locker_chunk[2] as $em_Buffer){
			
			$MID = str_pad(self::trimID($em_Buffer,16,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,20,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			$item['tab2']['pos'][]="x".self::getData($em_Buffer,0,2)."y".self::getData($em_Buffer,4,2);
			$item['tab2']['X'][]=self::getData($em_Buffer,0,2);
			$item['tab2']['Y'][]=self::getData($em_Buffer,4,2);
			$item['tab2']['MID'][]=$MID;
			$item['tab2']['SID'][]=$SID;
			$item['tab2']['QTY'][]=self::getOffset($em_Buffer,82,80);
			$item['tab2']['MEM'][]=$em_Buffer;
			$item['tab2']['ItemName'][]=$csv->item_offset[$address]["strName"];
			$item['tab2']['emItemType'][]=$csv->item_offset[$address]["emItemType"];
			$item['tab2']['emDrug'][]=$csv->item_offset[$address]["emDrug"];
			$item['tab2']['emLevel'][]=$csv->item_offset[$address]["emLevel"];
			$item['tab2']['DEF_GRADE'][]=self::getData($em_Buffer,90,2);//+damage upgrade///
			$item['tab2']['DMG_GRADE'][]=self::getData($em_Buffer,92,2); //+defense upgrade//
			$item['tab2']['ELEC'][]	=self::getData($em_Buffer,98,2); //+elec upgrade//
			$item['tab2']['FIRE'][]	=self::getData($em_Buffer,94,2); //+fire upgrade//
			$item['tab2']['ICE'][]		=self::getData($em_Buffer,96,2); //+ice upgrade//
			$item['tab2']['POISON'][]	=self::getData($em_Buffer,100,2); //+poison upgrade//
			$item['tab2']['WIND'][]	=self::getData($em_Buffer,102,2); //+wind upgrade//
			$item['tab2']['Opt1'][]	=self::getData($em_Buffer,104,2);
			$item['tab2']['Opt2'][]	=self::getData($em_Buffer,108,2);
			$item['tab2']['Opt3'][]	=self::getData($em_Buffer,106,2);
			$item['tab2']['Opt4'][]	=self::getData($em_Buffer,110,2);
			$item['tab2']['Val1'][]	=self::getOffset($em_Buffer,114,112);
			$item['tab2']['Val2'][]	=self::getOffset($em_Buffer,122,120);
			$item['tab2']['Val3'][]	=self::getOffset($em_Buffer,118,116);
			$item['tab2']['Val4'][]	=self::getOffset($em_Buffer,126,124);
			
		}
		}else{ 
				$item['tab2']['pos']="none";
		}
		if($locker_3_size!=0){
		foreach($locker_chunk[3] as $em_Buffer){
			$MID = str_pad(self::trimID($em_Buffer,16,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,20,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			$item['tab3']['pos'][]="x".self::getData($em_Buffer,0,2)."y".self::getData($em_Buffer,4,2);
			$item['tab3']['X'][]=self::getData($em_Buffer,0,2);
			$item['tab3']['Y'][]=self::getData($em_Buffer,4,2);
			$item['tab3']['MID'][]=$MID;
			$item['tab3']['SID'][]=$SID;
			$item['tab3']['QTY'][]=self::getOffset($em_Buffer,82,80);
			$item['tab3']['MEM'][]=$em_Buffer;
			$item['tab3']['ItemName'][]=$csv->item_offset[$address]["strName"];
			$item['tab3']['emItemType'][]=$csv->item_offset[$address]["emItemType"];
			$item['tab3']['emDrug'][]=$csv->item_offset[$address]["emDrug"];
			$item['tab3']['emLevel'][]=$csv->item_offset[$address]["emLevel"];
			$item['tab3']['DEF_GRADE'][]=self::getData($em_Buffer,90,2);//+damage upgrade///
			$item['tab3']['DMG_GRADE'][]=self::getData($em_Buffer,92,2); //+defense upgrade//
			$item['tab3']['ELEC'][]	=self::getData($em_Buffer,98,2); //+elec upgrade//
			$item['tab3']['FIRE'][]	=self::getData($em_Buffer,94,2); //+fire upgrade//
			$item['tab3']['ICE'][]		=self::getData($em_Buffer,96,2); //+ice upgrade//
			$item['tab3']['POISON'][]	=self::getData($em_Buffer,100,2); //+poison upgrade//
			$item['tab3']['WIND'][]	=self::getData($em_Buffer,102,2); //+wind upgrade//
			$item['tab3']['Opt1'][]	=self::getData($em_Buffer,104,2);
			$item['tab3']['Opt2'][]	=self::getData($em_Buffer,108,2);
			$item['tab3']['Opt3'][]	=self::getData($em_Buffer,106,2);
			$item['tab3']['Opt4'][]	=self::getData($em_Buffer,110,2);
			$item['tab3']['Val1'][]	=self::getOffset($em_Buffer,114,112);
			$item['tab3']['Val2'][]	=self::getOffset($em_Buffer,122,120);
			$item['tab3']['Val3'][]	=self::getOffset($em_Buffer,118,116);
			$item['tab3']['Val4'][]	=self::getOffset($em_Buffer,126,124);
			
		}
		}else{ 
				$item['tab3']['pos']="none";
		}
		if($locker_4_size!=0){
		foreach($locker_chunk[4] as $em_Buffer){
			$MID = str_pad(self::trimID($em_Buffer,16,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,20,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			$item['tab4']['pos'][]="x".self::getData($em_Buffer,0,2)."y".self::getData($em_Buffer,4,2);
			$item['tab4']['X'][]=self::getData($em_Buffer,0,2);
			$item['tab4']['Y'][]=self::getData($em_Buffer,4,2);
			$item['tab4']['MID'][]=$MID;
			$item['tab4']['SID'][]=$SID;
			$item['tab4']['QTY'][]=self::getOffset($em_Buffer,82,80);
			$item['tab4']['MEM'][]=$em_Buffer;
			$item['tab4']['ItemName'][]=$csv->item_offset[$address]["strName"];
			$item['tab4']['emItemType'][]=$csv->item_offset[$address]["emItemType"];
			$item['tab4']['emDrug'][]=$csv->item_offset[$address]["emDrug"];
			$item['tab4']['emLevel'][]=$csv->item_offset[$address]["emLevel"];
			$item['tab4']['DEF_GRADE'][]=self::getData($em_Buffer,90,2);//+damage upgrade///
			$item['tab4']['DMG_GRADE'][]=self::getData($em_Buffer,92,2); //+defense upgrade//
			$item['tab4']['ELEC'][]	=self::getData($em_Buffer,98,2); //+elec upgrade//
			$item['tab4']['FIRE'][]	=self::getData($em_Buffer,94,2); //+fire upgrade//
			$item['tab4']['ICE'][]		=self::getData($em_Buffer,96,2); //+ice upgrade//
			$item['tab4']['POISON'][]	=self::getData($em_Buffer,100,2); //+poison upgrade//
			$item['tab4']['WIND'][]	=self::getData($em_Buffer,102,2); //+wind upgrade//
			$item['tab4']['Opt1'][]	=self::getData($em_Buffer,104,2);
			$item['tab4']['Opt2'][]	=self::getData($em_Buffer,108,2);
			$item['tab4']['Opt3'][]	=self::getData($em_Buffer,106,2);
			$item['tab4']['Opt4'][]	=self::getData($em_Buffer,110,2);
			$item['tab4']['Val1'][]	=self::getOffset($em_Buffer,114,112);
			$item['tab4']['Val2'][]	=self::getOffset($em_Buffer,122,120);
			$item['tab4']['Val3'][]	=self::getOffset($em_Buffer,118,116);
			$item['tab4']['Val4'][]	=self::getOffset($em_Buffer,126,124);
			
		}
		}else{ 
				$item['tab4']['pos']="none";
		}
		if($locker_5_size!=0){
		foreach($locker_chunk[5] as $em_Buffer){
			$MID = str_pad(self::trimID($em_Buffer,16,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,20,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			$item['tab5']['pos'][]="x".self::getData($em_Buffer,0,2)."y".self::getData($em_Buffer,4,2);
			$item['tab5']['X'][]=self::getData($em_Buffer,0,2);
			$item['tab5']['Y'][]=self::getData($em_Buffer,4,2);
			$item['tab5']['MID'][]=$MID;
			$item['tab5']['SID'][]=$SID;
			$item['tab5']['QTY'][]=self::getOffset($em_Buffer,82,80);
			$item['tab5']['MEM'][]=$em_Buffer;
			$item['tab5']['ItemName'][]=$csv->item_offset[$address]["strName"];
			$item['tab5']['emItemType'][]=$csv->item_offset[$address]["emItemType"];
			$item['tab5']['emDrug'][]=$csv->item_offset[$address]["emDrug"];
			$item['tab5']['emLevel'][]=$csv->item_offset[$address]["emLevel"];
			$item['tab5']['DEF_GRADE'][]=self::getData($em_Buffer,90,2);//+damage upgrade///
			$item['tab5']['DMG_GRADE'][]=self::getData($em_Buffer,92,2); //+defense upgrade//
			$item['tab5']['ELEC'][]	=self::getData($em_Buffer,98,2); //+elec upgrade//
			$item['tab5']['FIRE'][]	=self::getData($em_Buffer,94,2); //+fire upgrade//
			$item['tab5']['ICE'][]		=self::getData($em_Buffer,96,2); //+ice upgrade//
			$item['tab5']['POISON'][]	=self::getData($em_Buffer,100,2); //+poison upgrade//
			$item['tab5']['WIND'][]	=self::getData($em_Buffer,102,2); //+wind upgrade//
			$item['tab5']['Opt1'][]	=self::getData($em_Buffer,104,2);
			$item['tab5']['Opt2'][]	=self::getData($em_Buffer,108,2);
			$item['tab5']['Opt3'][]	=self::getData($em_Buffer,106,2);
			$item['tab5']['Opt4'][]	=self::getData($em_Buffer,110,2);
			$item['tab5']['Val1'][]	=self::getOffset($em_Buffer,114,112);
			$item['tab5']['Val2'][]	=self::getOffset($em_Buffer,122,120);
			$item['tab5']['Val3'][]	=self::getOffset($em_Buffer,118,116);
			$item['tab5']['Val4'][]	=self::getOffset($em_Buffer,126,124);
			
		}
		}else{ 
			$item['tab5']['pos']="none";
		}
		$item['gold']=number_format($this->_usermoney);
		self::pushJSON($item); 
	}
	public function trimID($a,$b,$c){
		$b=substr($a,$b,$c);
		$c=str_split($b,2);
		$d=$c[1].$c[0];
		return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}



	
}