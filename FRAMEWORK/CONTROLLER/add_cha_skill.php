<?php

ini_set ("odbc.defaultlrl", "6553611");
class add_cha_skill extends controller{
    private $ChaNum;
	private $cha_skill_F;
	private $skill_lvl;
	private $_chaskills;
    private $requirement;
	private $_buffer_memory;
    function __construct($requirement,$data=null) {
			$this->cha_skill_F=$data['skill'];
			$this->skill_lvl=$data['skill_lvl'];
            $this->ChaNum=$data['ChaNum'];
            $this->requirement=$requirement;
			
			self::FetchSkill();
			if(self::process_addSkill()){
				self::SaveMode();
				self::errorCODE(2);
			}else{
				self::errorCODE(1);
			}
          
    }
    private function FetchSkill(){
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaSkills";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_chaskills=bin2hex($result[0]['ChaSkills']);
				
				
            }
           
    }
	private function process_addSkill(){
			$temp_M = "0800000001000100";
			$skill_memory=$this->_chaskills;
			$skill_header=self::getMemory($skill_memory,0,24);
			$skill_old_tSize=self::getData($skill_header,16,2);
			$skill_add_tSize = count($this->cha_skill_F);
			$skill_size=strlen($skill_memory);
			$SKILL_FILE=substr($skill_memory,24,$skill_size);
			$skill_buffer_mem= str_split($SKILL_FILE,16);
			
			$skill_new_tSize=$skill_add_tSize+$skill_old_tSize;
			$skill_new_tSize=self::digitz($skill_new_tSize);
			$skill_temp=array();
			$az=0;
			$skill_header_New_H = self::updateoffset($skill_header,$skill_new_tSize,16,2);
			foreach($this->cha_skill_F as $em_buffer){
				$skill_f = explode("_",$em_buffer);
				$mid = self::digitz($skill_f[0]);
				$sid = self::digitz($skill_f[1]);
				$lvl = self::digitz($this->skill_lvl);
				$temp_buffer=self::updateoffset("0800000001004700",$mid,0,2);
				$temp_buffer=self::updateoffset($temp_buffer,$sid,4,2);
				$temp_buffer=self::updateoffset($temp_buffer,$lvl,8,2);
				$skill_temp[$az]=$temp_buffer;
				$az++;
				
			}
			
			$this->_buffer_memory = $skill_header_New_H.implode($skill_buffer_mem).implode($skill_temp);
			
		return true;	
	}	
	private function SaveMode(){
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaSkills=convert(varbinary(MAX),0x$this->_buffer_memory)",$filter);
       
	}
	public function updateoffset($a,$b,$c,$d){return substr_replace($a,$b,$c,$d);}
	public function digitz($a){$b=dechex($a);return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
}