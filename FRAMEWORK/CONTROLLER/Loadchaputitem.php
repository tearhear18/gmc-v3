<?php
error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class Loadchaputitem extends controller{
    private $ChaNum;
	private $_ChaPutOnItems;
    private $requirement;
    function __construct($requirement,$data=null) {
		
            $this->ChaNum=$data['ChaNum'];
            $this->requirement=$requirement;
		
			self::fetchInventory();
            self::DecodeInventory();
          
    }
    private function fetchInventory(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaPutOnItems";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_ChaPutOnItems=bin2hex($result[0]['ChaPutOnItems']);
				
            }
           
    }
	
	private function DecodeInventory(){
		$csv = new item_csv;
		$csv->read();  //fireup csv and check for flash cache version :D 
		$inven_memory=$this->_ChaPutOnItems;
		$item_suit_header=self::getMemory($inven_memory,0,24);
		$item_suit_size=strlen($inven_memory);
		$item_suit_mem = self::getMemory($inven_memory,24,3384);
		
		$MEM_SIZE=(self::getData($item_suit_header,0,2)==20)? 160 :144;
		$item_suit_arr=str_split($item_suit_mem,$MEM_SIZE);
		
		
		$locker_chunk=array();
		foreach($item_suit_arr  as $em_Buffer){
			$MID = str_pad(self::trimID($em_Buffer,0,4),3,0,STR_PAD_LEFT);
			$SID = str_pad(self::trimID($em_Buffer,4,4),3,0,STR_PAD_LEFT);
			$address="IN_".$MID."_".$SID;
			
			if($MID==65535){
				$locker_chunk['wear']['ItemName'][]="NULL";
				
			}else{
				
				$locker_chunk['wear']['ItemName'][]=$csv->item_offset[$address]["strName"];
			}
			
			$locker_chunk['wear']['MEM'][]=$em_Buffer;
			$locker_chunk['wear']['MID'][]=$MID;
			$locker_chunk['wear']['SID'][]=$SID;
			//$locker_chunk['wear']['X'][]="N/A";
			//$locker_chunk['wear']['Y'][]="N/A";
			//$locker_chunk['wear']['item_type'][]=$csv->item_offset[$address]["emItemType"];
			//$locker_chunk['wear']['item_type'][]=$csv->item_offset[$address]["emDrug"];
			//$locker_chunk['wear']['item_type'][]=$csv->item_offset[$address]["emSuit"];
			//$locker_chunk['wear']['item_type'][]=$csv->item_offset[$address]["emLevel"];
			$locker_chunk['wear']['DEF_GRADE'][]=self::getData($em_Buffer,76,2);//+damage upgrade///
			$locker_chunk['wear']['DMG_GRADE'][]=self::getData($em_Buffer,74,2); //+defense upgrade//
			$locker_chunk['wear']['ELEC'][]	=self::getData($em_Buffer,82,2); //+elec upgrade//
			$locker_chunk['wear']['FIRE'][]	=self::getData($em_Buffer,78,2); //+fire upgrade//
			$locker_chunk['wear']['ICE'][]		=self::getData($em_Buffer,80,2); //+ice upgrade//
			$locker_chunk['wear']['POISON'][]	=self::getData($em_Buffer,84,2); //+poison upgrade//
			$locker_chunk['wear']['WIND'][]	=self::getData($em_Buffer,86,2); //+wind upgrade//
			$locker_chunk['wear']['Opt1'][]	=self::getData($em_Buffer,88,2);
			$locker_chunk['wear']['Opt2'][]	=self::getData($em_Buffer,90,2);
			$locker_chunk['wear']['Opt3'][]	=self::getData($em_Buffer,92,2);
			$locker_chunk['wear']['Opt4'][]	=self::getData($em_Buffer,94,2);
			$locker_chunk['wear']['Val1'][]	=self::getOffset($em_Buffer,98,96);
			$locker_chunk['wear']['Val2'][]	=self::getOffset($em_Buffer,102,100);
			$locker_chunk['wear']['Val3'][]	=self::getOffset($em_Buffer,106,104);
			$locker_chunk['wear']['Val4'][]	=self::getOffset($em_Buffer,110,108);
			$locker_chunk['wear']['QTY'][]	=self::getOffset($em_Buffer,66,64);
			
		}
		
		
		self::pushJSON($locker_chunk); 
	}
	public function trimID($a,$b,$c){
		$b=substr($a,$b,$c);
		$c=str_split($b,2);
		$d=$c[1].$c[0];
		return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}



	
}