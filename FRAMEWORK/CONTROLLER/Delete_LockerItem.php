<?php
//error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class Delete_LockerItem extends controller{
    private $UserNum;
	private $memory;
	private $tab;
	private $_UserInven;
	private $_buffer_memory="";
    private $requirement;
    function __construct($requirement,$data=null) {
			
            $this->UserNum=$data['UserNum'];
            $this->tab=$data['tab'];
			$this->memory=$data['nofilter']['memory'];
            $this->requirement=$requirement;
			
			self::fetchInventory();
			if(self::Process_DeleteItem()){
				self::SaveMode();
				self::errorCode(2);
			}else{
				self::errorCode(1);
			}
			
			
    }
    private function fetchInventory(){
          
            $model= $this->requireModel('UserInven',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="UserInven";
    
            //add filters//
            $filter="UserNum=$this->UserNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_UserInven=bin2hex($result[0]['UserInven']);
			
            }
           
    }
	
	private function Process_DeleteItem(){
		$inven_memory=$this->_UserInven;
		$this->_userinven=null;
		//////////////////////////////locker1
		$locker_version=self::getMemory($inven_memory,0,8);
		$locker_1_head=self::getMemory($inven_memory,8,24);
		$MEM_SIZE=(self::getData($locker_1_head,0,2)==20)? 176 :160;
		$locker_1_size=hexdec(self::getMemory($locker_1_head,16,2));
		$locker_1_length=$MEM_SIZE*$locker_1_size;
		if($locker_1_size!=0){
			$locker_1_memory=self::getMemory($inven_memory,32,$locker_1_length);
			$locker_chunk[1]=str_split($locker_1_memory,$MEM_SIZE);
			
		}else{
			$locker_chunk[1]="";
		}
		/////////////////////////////locker2
		$mem_location=32+$locker_1_length;
		$locker_2_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_2_size=hexdec(self::getMemory($locker_2_head,16,2));
		$locker_2_length=$MEM_SIZE*$locker_2_size;
		if($locker_2_size!=0){
			
			$locker_2_memory=self::getMemory($inven_memory,$mem_location+24,$locker_2_length);
			$locker_chunk[2]=str_split($locker_2_memory,$MEM_SIZE);
			
		}else{
			$locker_chunk[2]="";
		}
		/////////////////////////////locker3
		$mem_location=32+$locker_1_length+24+$locker_2_length;
		$locker_3_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_3_size=hexdec(self::getMemory($locker_3_head,16,2));
		$locker_3_length=$MEM_SIZE*$locker_3_size;
		if($locker_3_size!=0){
			$locker_3_memory=self::getMemory($inven_memory,$mem_location+24,$locker_3_length);
			$locker_chunk[3]=str_split($locker_3_memory,$MEM_SIZE);
		}else{
			$locker_chunk[3]="";
		}
		//////////////////////////////locker4
		$mem_location=32+$locker_1_length+24+$locker_2_length+24+$locker_3_length;
		$locker_4_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_4_size=hexdec(self::getMemory($locker_4_head,16,2));
		$locker_4_length=$MEM_SIZE*$locker_4_size;
		if($locker_4_size!=0){
			$locker_4_memory=self::getMemory($inven_memory,$mem_location+24,$locker_4_length);
			$locker_chunk[4]=str_split($locker_4_memory,$MEM_SIZE);
		}else{
			$locker_chunk[4]="";
		}
		//////////////////////////////locker5
		$mem_location=32+$locker_1_length+24+$locker_2_length+24+$locker_3_length+24+$locker_4_length;
		$locker_5_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_5_size=hexdec(self::getMemory($locker_5_head,16,2));
		$locker_5_length=$MEM_SIZE*$locker_5_size;
		if($locker_5_size!=0){
			$locker_5_memory=self::getMemory($inven_memory,$mem_location+24,$locker_5_length);
			$locker_chunk[5]=str_split($locker_5_memory,$MEM_SIZE);
		}else{
			$locker_chunk[5]="";
		}
		
		$temp_mem=$locker_chunk[$this->tab];
		if (in_array($this->memory,$temp_mem)) {   //checking memory existence in buffer 
			$temp_mem = array_flip($temp_mem);  	//reverse data key to value
			$indexOF=$temp_mem[$this->memory];		
			unset($locker_chunk[$this->tab][$indexOF]); //now we can unset offset by value in array
			//now here is the repacking method// there will be a work around for this process but duh!.. who cares:D
			//work around? reformat the decoding method make array instead of variable duh lot of work :D//
			//this code repack the binaries in form of tab since we cannot combine those shitz in single piece code without evaluating  each tab :(//
			switch($this->tab){
				case 1:
					$n_locker_H=self::updateoffset($locker_1_head,self::digitz($locker_1_size-1),16,2);
					$this->_buffer_memory=$locker_version.$n_locker_H.implode($locker_chunk[1]);
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$locker_4_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					
					break;
				case 2:
					$n_locker_H=self::updateoffset($locker_2_head,self::digitz($locker_2_size-1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[2]);
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$locker_4_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					break;
				case 3:
					$n_locker_H=self::updateoffset($locker_3_head,self::digitz($locker_3_size-1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[3]);
					$this->_buffer_memory.=$locker_4_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					break;
				case 4:
					$n_locker_H=self::updateoffset($locker_4_head,self::digitz($locker_4_size-1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[4]);
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					break;
				case 5:
					$n_locker_H=self::updateoffset($locker_5_head,self::digitz($locker_5_size-1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[4]);
					break;
			}
			
			return true;
		}else{
			
			return false;
		}
		
	}

	private function SaveMode(){
		$model= $this->requireModel('UserInven',$this->requirement['mssql']);
		$filter="UserNum=$this->UserNum";
        $model->updateByAttribute("UserInven=convert(varbinary(MAX),0x$this->_buffer_memory)",$filter);
       
	}
	public function trimID($a,$b,$c){
		$b=substr($a,$b,$c);
		$c=str_split($b,2);
		$d=$c[1].$c[0];
		return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}
	public function digitz($a){
		$b=dechex($a);
		return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);
	}

	function updateoffset($a,$b,$c,$d){
		return substr_replace($a,$b,$c,$d);
	}
	
}