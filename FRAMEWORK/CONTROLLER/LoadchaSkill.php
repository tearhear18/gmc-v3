<?php
error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class LoadchaSkill extends controller{
    private $ChaNum;
	private $_chaskills;
	private $_chaClass;
    private $requirement;
    function __construct($requirement,$data=null) {

            $this->ChaNum=$data['ChaNum'];
            $this->requirement=$requirement;
			
			self::FetchSkill();
            self::DecodeSkill();
          
    }
    private function FetchSkill(){
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaSkills,ChaClass";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_chaskills=bin2hex($result[0]['ChaSkills']);
				$this->_chaClass=$result[0]['ChaClass'];
				
            }
           
    }
	private function DecodeSkill(){

			$skill = new skill_sz;
			
			$skill->read();
			

			$skill_memory=$this->_chaskills;
			$skill_header=self::getMemory($skill_memory,0,24);
			$skill_size=strlen($skill_memory);
			$SKILL_FILE=substr($skill_memory,24,$skill_size);
			$skill_buffer_mem= str_split($SKILL_FILE,16);
			$DATA=array();
				$DATA['skill']['svr']=$skill->skillsZ;
				foreach($skill_buffer_mem as $em_Buffer){
				
					$MID=str_pad(self::getData($em_Buffer,0,2),3,0,STR_PAD_LEFT);
					$SID=self::getData($em_Buffer,4,2);
					$DATA['skill']['mid'][]=$MID;
					$DATA['skill']['sid'][]=$SID;
					$DATA['skill']['name'][]=$skill->skillsZ[$MID][$SID];
					$DATA['skill']['lvl'][]=self::getData($em_Buffer,8,2);
					
				}
					$DATA['chaClass']=$this->_chaClass;
					self::pushJSON($DATA);
			
	}	
	
	public function trimID($a,$b,$c){
	$b=substr($a,$b,$c);
	$c=str_split($b,2);
	$d=$c[1].$c[0];
	return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}
	
}