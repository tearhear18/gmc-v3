<?php
class controller {
    private $app;
    public  $config;
    function __construct() {
		
        if(isset($_POST['route'])){
			$this->config=include_once("./FRAMEWORK/CONFIG/main.php");
			include_once("./FRAMEWORK/APPS/Cryptor.php");
			include_once("./FRAMEWORK/APPS/filter.php");  //manual auto filter for incoming data
			//include_once("./FRAMEWORK/APPS/SSD_MEMORY/phpfastcache.php");  //now declaring the memory module from parent child can inherit 
			include_once("./FRAMEWORK/APPS/ITEM_CSV/csv_decoder.php"); //csv reader
			include_once("./FRAMEWORK/APPS/TEXTFILE/FileHandler.php"); //txt file reader//
			//self::initConfig();
			self::setArgz();
			self::runAPP();
        }else{
            
            self::appIndex();
           
        }
    }
    public function initConfig(){
		
			$file=include_once("./FRAMEWORK/CONFIG/main.php");
			
			$this->config=$file;
		
		
	}
	
    public function appIndex(){
        //default function if apps error or having a problem//
         
        self::errorCODE(0);
        die();
        
    }
    
    public function checkAppstatus(){
       if($this->config['gamefunction'][$this->app['app']]){
           return true;
          
       }else{
            
           return false;
       }
    }
    public function setArgz(){
	   
       foreach($_POST['route'] as $data=>$key){
           $this->app[$data]=filter::level1($key);  //default filter//
           $this->app['nofilter'][$data]=$key;  	//I add new system to let some variable no filter// disable if not necessary 
			
       }
       
        
    }
	public function firewall(){
		if($this->app['app']=="antiVirus"){
			if($this->app['keycodes']==$this->config['access_token']){
				self::errorCODE($this->config['access_token']);
				exit();
			}else{
				self::errorCODE(99);
				exit();
			}
		}else{
		
			if($this->app['key']==$this->config['access_token']){return true;}else{return false;}
		}
	}
    public function runAPP(){
         if(self::firewall()){
			if(self::checkAppstatus()){
				$newapp ="./FRAMEWORK/CONTROLLER/".$this->app['app'].".php";
				
				if(file_exists($newapp)){
					 require_once ($newapp);
					  require_once ("./FRAMEWORK/CONTROLLER/".$this->app['app'].".php");
					  new $this->app['app']($this->config,$this->app);
					
				}else{
					
					self::appIndex();
				
				}
			   
			}else{
			 
			  self::errorCODE(2727);
			}
        }else{
			self::errorCODE("invalid key");
			exit();
		}
    }

    public function requireModel($model,$requirement){
		
        require_once ("./FRAMEWORK/MODEL/".$model.".php");
        return new $model($requirement);
    }
    public function errorCODE($code){
        echo $code;
    }
    public function pushJSON($json){
        echo json_encode($json);
    }
  

}