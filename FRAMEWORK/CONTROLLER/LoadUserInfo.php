<?php
class LoadUserInfo extends controller{
    private $UserNum;
	private $data=array();
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->UserNum=$data['UserNum'];
            $this->requirement=$requirement;
			self::lookup();
            
    }
    private function lookup(){
          
            $model= $this->requireModel('UserInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="UserID,UserNum,UserType,SGNum,SvrNum,ChaRemain,ChaTestRemain,UserEmail";
    
            //add filters//
            $filter="UserNum=$this->UserNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
            }else{
				$data=array();
				$this->data['UserInfo']=$result;
				self::fetchIP($result[0]['UserID']);
				self::pushJSON($this->data); 
                
            }
           
    }
	private function fetchIP($id){
		$model= $this->requireModel('LogLogin',$this->requirement['mssql']);
		 $col ="TOP 1 LogIpAddress";
		 $filter="UserID='$id' AND LogIpAddress!='NULL' Order By LoginNum Desc";
		 $result=$model->findByAttribute($col,$filter);
		 if($result==null){
			 $this->data['ip']="No Ip record";
		 }else{
			 $this->data['ip']=$result[0]['LogIpAddress'];
		 }
		
	}
	
}