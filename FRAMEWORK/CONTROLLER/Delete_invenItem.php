<?php
error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class Delete_invenItem extends controller{
    private $ChaNum;
	private $memory;
	private $_chainven;
	private $_buffer_memory;
    private $requirement;
    function __construct($requirement,$data=null) {
			
            $this->ChaNum=$data['ChaNum'];
			$this->memory=$data['nofilter']['memory'];
            $this->requirement=$requirement;
			
			self::fetchInventory();
			if(self::Process_DeleteItem()){
				self::SaveMode();
				self::errorCode(2);
			}else{
				self::errorCode(1);
			}
			
			
    }
    private function fetchInventory(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaInven";
    
            //add filters//
            $filter="ChaNum=$this->ChaNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_chainven=bin2hex($result[0]['ChaInven']);
			
            }
           
    }
	
	private function Process_DeleteItem(){
		
		$CHA_INVEN=$this->_chainven;
		$this->_chainven=null;  //destroy old mem//
		
		$INVEN_HEAD = substr($CHA_INVEN,0,24);   
		$INVEN_SIZE = hexdec(self::getMemory($INVEN_HEAD,16,2));         	//count item in inventory//
		$INVEN_SIZE-=1;
		$FILE_SIZE = strlen($CHA_INVEN);					//count the total hex digits
		$ITEM = substr($CHA_INVEN,24,$FILE_SIZE);
		$MEM_SIZE=(self::getData($INVEN_HEAD,0,2)==20)? 176 :160; //sub versioning//
		$ITEM_LIST = str_split($ITEM,$MEM_SIZE);
		$INVEN_HEAD_NEW=self::updateoffset($INVEN_HEAD,self::digitz($INVEN_SIZE),16,2);
		
		if (in_array($this->memory,$ITEM_LIST)) {   //checking memory existence in buffer 
			$ITEM_LIST = array_flip($ITEM_LIST);  	//reverse data ket to value
			unset($ITEM_LIST[$this->memory]);		//now we can unset offset by value in array
			$ITEM_LIST = array_flip($ITEM_LIST);	//lets make it back to proper array position
		
			
			$this->_buffer_memory = $INVEN_HEAD_NEW.implode($ITEM_LIST);
			
			$ITEM_LIST=null;
			return true;
		}else{
			
			return false;
		}
		
		
	}
	private function SaveMode(){
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaInven=convert(varbinary(MAX),0x$this->_buffer_memory)",$filter);
       
	}
	public function trimID($a,$b,$c){
		$b=substr($a,$b,$c);
		$c=str_split($b,2);
		$d=$c[1].$c[0];
		return hexdec($d);
	}
	public function getData($a,$b,$c){
		return hexdec(substr($a,$b,$c));	
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getOffset($a,$b,$c){
		$f=substr($a,$b,2)."".substr($a,$c,2);
		return hexdec($f);
	}
	public function digitz($a){
		$b=dechex($a);
		return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);
	}

	function updateoffset($a,$b,$c,$d){
		return substr_replace($a,$b,$c,$d);
	}
	
}