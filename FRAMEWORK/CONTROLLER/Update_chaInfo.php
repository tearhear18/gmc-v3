<?php
class Update_chaInfo extends controller{
	var $ChaPowS;
	var $ChaStrongS;
	var $ChaMoneyS;
	var $ChaName;
	var $ChaDexS;
	var $ChaStrengthS;
	var $ChaVotePoints;
	var $ChaSpiritS;
	var $ChaAgilityS;
	var $ChaPremiumPointS;
	var $cha_classD;
	var $ChaLevelS;
	var $ChaRebornS;
	var $Cha_schoolD;
	var $ChaSkillPointS;
	var $ChaStRemainS;
	var $ChaNum;
	var $empty_SKL = "000100000800000000000000";
    private $requirement;
	
	private $_ChaName;
	private $_ChaClass;
	
    function __construct($requirement,$data=null) {
			
			$this->requirement=$requirement;  
			$this->ChaPowS			=$data['ChaPowS'];
			$this->ChaStrongS		=$data['ChaStrongS'];
			$this->ChaMoneyS		=$data['ChaMoneyS'];
			$this->ChaName			=$data['ChaName'];
			$this->ChaDexS			=$data['ChaDexS'];
			$this->ChaStrengthS		=$data['ChaStrengthS'];
			$this->ChaVotePoints	=$data['ChaVotePoints'];
			$this->ChaSpiritS		=$data['ChaSpiritS'];
			$this->ChaAgilityS		=$data['ChaAgilityS'];
			$this->ChaPremiumPointS	=$data['ChaPremiumPointS'];
			$this->cha_classD		=$data['cha_classD'];
			$this->ChaLevelS		=$data['ChaLevelS'];
			$this->ChaRebornS		=$data['ChaRebornS'];
			$this->Cha_schoolD		=$data['Cha_schoolD'];
			$this->ChaSkillPointS	=$data['ChaSkillPointS'];
			$this->ChaStRemainS		=$data['ChaStRemainS'];
			$this->ChaNum			=$data['ChaNum'];
			
			self::getOldChaInfo();
			
			if($this->_ChaName==$this->ChaName){
				
				if(self::checkClass()){
					//proceed to saving mode
					
						self::saveMode();
						self::errorCODE(2);
				}else{
					
						self::saveMode2();
						self::errorCODE(2);
					//empty skill command
				}
			}else{
				
				if(self::verifyName()){
					if(self::checkClass()){
						//proceed to saving mode with change name
						self::saveMode2();
						self::errorCODE(2);
					}else{
						//empty skill command with change name
						self::saveMode3();
						self::errorCODE(2);
					}
				}else{
					self::errorCODE(1);  //name not available
				}
			}
            
    }
    private function getOldChaInfo(){
		 $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            $col ="ChaClass,ChaName,ChaOnline";
    
            $filter="ChaNum=$this->ChaNum";
    
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
				$this->_ChaName		=$result[0]['ChaName'];
				$this->_ChaClass	=$result[0]['ChaClass'];
				if($result[0]['ChaOnline']==1){
					self::errorCODE(3);
					exit();
				}
            }
           
	}
	private function verifyName(){
		 $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            $col ="ChaName";
    
            $filter="ChaName='$this->ChaName'";
    
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
               return true;
            }else{
				return false;
            }
           
	}
	private function checkClass(){
		if($this->_ChaClass==$this->cha_classD){
			return true;
		}else{
			//this detect if change of class is only between male or female no need wiping skills//
			switch($this->_ChaClass){
				case 1:
					return ($this->cha_classD==64)? true : false;
					break;
				case 64:
					return ($this->cha_classD==1)? true : false;
					break;
				case 2:
					return ($this->cha_classD==128)? true : false;
					break;
				case 128:
					return ($this->cha_classD==2)? true : false;
					break;
				case 256:
					return ($this->cha_classD==4)? true : false;
					break;
				case 4:
					return ($this->cha_classD==256)? true : false;
					break;
				case 8:
					return ($this->cha_classD==512)? true : false;
					break;
				case 512:
					return ($this->cha_classD==8)? true : false;
					break;
				case 16:
					return ($this->cha_classD==32)? true : false;
					break;
				case 32:
					return ($this->cha_classD==16)? true : false;
					break;
				case 1024:
					return ($this->cha_classD==2048)? true : false;
					break;
				case 2048:
					return ($this->cha_classD==1024)? true : false;
					break;
				case 4096:
					return ($this->cha_classD==8192)? true : false;
					break;
				case 8192:
					return ($this->cha_classD==8192)? true : false;
					break;
			}
		}
		
	}
	private function saveMode(){
	
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaPower=$this->ChaPowS,ChaStrong=$this->ChaStrongS,ChaMoney=$this->ChaMoneyS,ChaDex=$this->ChaDexS,ChaStrength=$this->ChaStrengthS,ChaVotePoint=$this->ChaVotePoints,ChaSpirit=$this->ChaSpiritS,ChaAgility=$this->ChaAgilityS,ChaPremiumPoint=$this->ChaPremiumPointS,ChaClass=$this->cha_classD,ChaLevel=$this->ChaLevelS,ChaReborn=$this->ChaRebornS,ChaSchool=$this->Cha_schoolD,ChaSkillPoint=$this->ChaSkillPointS,ChaStRemain=$this->ChaStRemainS",$filter);
	}
	private function saveMode2(){
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaName='$this->ChaName',ChaSkills=convert(varbinary(MAX),0x$this->empty_SKL),ChaPower=$this->ChaPowS,ChaStrong=$this->ChaStrongS,ChaMoney=$this->ChaMoneyS,ChaDex=$this->ChaDexS,ChaStrength=$this->ChaStrengthS,ChaVotePoint=$this->ChaVotePoints,ChaSpirit=$this->ChaSpiritS,ChaAgility=$this->ChaAgilityS,ChaPremiumPoint=$this->ChaPremiumPointS,ChaClass=$this->cha_classD,ChaLevel=$this->ChaLevelS,ChaReborn=$this->ChaRebornS,ChaSchool=$this->Cha_schoolD,ChaSkillPoint=$this->ChaSkillPointS,ChaStRemain=$this->ChaStRemainS",$filter);
	}
	private function saveMode3(){
		$model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
		$filter="ChaNum=$this->ChaNum";
        $model->updateByAttribute("ChaName='$this->ChaName',ChaPower=$this->ChaPowS,ChaStrong=$this->ChaStrongS,ChaMoney=$this->ChaMoneyS,ChaDex=$this->ChaDexS,ChaStrength=$this->ChaStrengthS,ChaVotePoint=$this->ChaVotePoints,ChaSpirit=$this->ChaSpiritS,ChaAgility=$this->ChaAgilityS,ChaPremiumPoint=$this->ChaPremiumPointS,ChaClass=$this->cha_classD,ChaLevel=$this->ChaLevelS,ChaReborn=$this->ChaRebornS,ChaSchool=$this->Cha_schoolD,ChaSkillPoint=$this->ChaSkillPointS,ChaStRemain=$this->ChaStRemainS",$filter);
	}
}