<?php
class DeleteitemBank extends controller{
    private $UserNum;
    private $ProductNum=array();
	
	private $_username;
    private $requirement;
    function __construct($requirement,$data=null) {
			$this->requirement=$requirement;
            $this->UserNum=$data['UserNum'];
            $this->ProdutNum=$data['ProductNum'];
			
			self::getUserName();
			foreach($this->ProdutNum as $value){
				self::remove_item($value);
			}
            
    }
    
	private function getUserName(){
			$model= $this->requireModel('UserInfo',$this->requirement['mssql']);
            //declare here the data you want to pull out from the declared model //
            $col ="UserID";
            $filter="UserNum='{$this->UserNum}'";
			$username = $model->findByAttribute($col,$filter);
			$this->_username= $username[0]['UserID'];
			
		  
    }
	private function remove_item($purkey){
		$model= $this->requireModel('ShopPurchase',$this->requirement['mssql']);
		$filter1="PurKey='{$purkey}'";
        $model->updateByAttribute("PurFlag='1'",$filter1);
		
	}
}