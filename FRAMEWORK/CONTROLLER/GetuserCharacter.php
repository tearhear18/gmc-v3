<?php
class GetuserCharacter extends controller{
    private $UserNum;
	private $data=array();
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->UserNum=$data['UserNum'];
            $this->requirement=$requirement;
			self::lookup();
            
    }
    private function lookup(){
          
            $model= $this->requireModel('ChaInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ChaNum,ChaName,ChaOnline,ChaDeleted";
    
            //add filters//
            $filter="UserNum=$this->UserNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
            }else{
				
				self::pushJSON($result); 
                
            }
           
    }
	
	
}