<?php
//error_reporting(0);
ini_set ("odbc.defaultlrl", "6553611");
class add_item_locker extends controller{
    private $UserNum;
	private $memory;
	
	private $c_x;
	private $c_y;
	private $mid;
	private $sid;
	private $tab;
	
	private $pdmg;
	private $pdf;
	private $pqty;
	
	private $pfire;
	private $pice;
	private $pelec;
	private $ppoi;
	private $pwind;
	
	private $opt1;
	private $opt2;
	private $opt3;
	private $opt4;
	private $val1;
	private $val2;
	private $val3;
	private $val4;
	
	private $_userinven;
	private $_buffer_memory;
    private $requirement;
	
	private $D_mem_160 ="000000007365727300000f00ffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008010000000000";
	private $D_mem_176 ="000000007365727300000f00ffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008010000000000ff7f177cec060300";
	
    function __construct($requirement,$data=null) {
			
            $this->UserNum=$data['UserNum'];
			
			$this->pX		=self::trimoffset($data['pX']); //position X
			$this->pY		=self::trimoffset($data['pY']);
			$this->tab		=$data['tab'];
			
			$temp_address = $data['mid_sid'];
			$temp_address=explode("_",$this->request['item_ms_id']);
			
			$this->mid		=self::trimOpt($temp_address[1]);
			$this->sid		=self::trimOpt($temp_address[2]);
			
			$this->pdmg		=self::trimoffset($data['pdmg']);
			$this->pdf		=self::trimoffset($data['pdf']);
			$this->pqty		=self::trimOpt($data['pqty']);
			$this->pfire	=self::trimoffset($data['pfire']);
			$this->pice		=self::trimoffset($data['pice']);
			$this->pelec	=self::trimoffset($data['pelec']);
			$this->ppoi		=self::trimoffset($data['ppoi']);
			$this->pwind	=self::trimoffset($data['pwind']);
			$this->opt1		=self::trimoffset($data['opt1']);
			$this->opt2		=self::trimoffset($data['opt2']);
			$this->opt3		=self::trimoffset($data['opt3']);
			$this->opt4		=self::trimoffset($data['opt4']);
			$this->val1		=self::trimOpt($data['val1']);
			$this->val2		=self::trimOpt($data['val2']);
			$this->val3		=self::trimOpt($data['val3']);
			$this->val4		=self::trimOpt($data['val4']);
			
            $this->requirement=$requirement;
			
			self::fetchLocker();
			
			if(self::Process_AddItem()){
				self::SaveMode();
				self::errorCode(2);
			}else{
				self::errorCode(1);
			}
			
			
    }
    private function fetchLocker(){
          
            $model= $this->requireModel('UserInven',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="UserInven,UserMoney";
    
            //add filters//
            $filter="UserNum=$this->UserNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                self::errorCODE(1); 
				exit();
            }else{
			
				$this->_userinven=bin2hex($result[0]['UserInven']);
				$this->_usermoney=$result[0]['UserMoney'];
            }
           
    }
	private function Process_AddItem(){
		
		$inven_memory=$this->_userinven;
		
		$this->_userinven=null;
		//////////////////////////////locker1
		$locker_version=self::getMemory($inven_memory,0,8);
		$locker_1_head=self::getMemory($inven_memory,8,24);
		$MEM_SIZE=(self::getData($locker_1_head,0,2)==20)? 176 :160;
		$locker_1_size=hexdec(self::getMemory($locker_1_head,16,2));
		$locker_1_length=$MEM_SIZE*$locker_1_size;
		if($locker_1_size!=0){
			$locker_1_memory=self::getMemory($inven_memory,32,$locker_1_length);
			$locker_chunk[1]=str_split($locker_1_memory,$MEM_SIZE);
			
		}else{
			$locker_chunk[1]="";
		}
		/////////////////////////////locker2
		$mem_location=32+$locker_1_length;
		$locker_2_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_2_size=hexdec(self::getMemory($locker_2_head,16,2));
		$locker_2_length=$MEM_SIZE*$locker_2_size;
		if($locker_2_size!=0){
			
			$locker_2_memory=self::getMemory($inven_memory,$mem_location+24,$locker_2_length);
			$locker_chunk[2]=str_split($locker_2_memory,$MEM_SIZE);
			
		}else{
			$locker_chunk[2]="";
		}
		/////////////////////////////locker3
		$mem_location=32+$locker_1_length+24+$locker_2_length;
		$locker_3_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_3_size=hexdec(self::getMemory($locker_3_head,16,2));
		$locker_3_length=$MEM_SIZE*$locker_3_size;
		if($locker_3_size!=0){
			$locker_3_memory=self::getMemory($inven_memory,$mem_location+24,$locker_3_length);
			$locker_chunk[3]=str_split($locker_3_memory,$MEM_SIZE);
		}else{
			$locker_chunk[3]="";
		}
		//////////////////////////////locker4
		$mem_location=32+$locker_1_length+24+$locker_2_length+24+$locker_3_length;
		$locker_4_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_4_size=hexdec(self::getMemory($locker_4_head,16,2));
		$locker_4_length=$MEM_SIZE*$locker_4_size;
		if($locker_4_size!=0){
			$locker_4_memory=self::getMemory($inven_memory,$mem_location+24,$locker_4_length);
			$locker_chunk[4]=str_split($locker_4_memory,$MEM_SIZE);
		}else{
			$locker_chunk[4]="";
		}
		//////////////////////////////locker5
		$mem_location=32+$locker_1_length+24+$locker_2_length+24+$locker_3_length+24+$locker_4_length;
		$locker_5_head=self::getMemory($inven_memory,$mem_location,24);
		$locker_5_size=hexdec(self::getMemory($locker_5_head,16,2));
		$locker_5_length=$MEM_SIZE*$locker_5_size;
		if($locker_5_size!=0){
			$locker_5_memory=self::getMemory($inven_memory,$mem_location+24,$locker_5_length);
			$locker_chunk[5]=str_split($locker_5_memory,$MEM_SIZE);
		}else{
			$locker_chunk[5]="";
		}
		
			$ITEM_GRADE			=$this->pdmg.$this->pdf;
			$ITEM_ELEMENT		=$this->pfire.$this->pice.$this->pelec.$this->ppoi.$this->pwind;
			$ITEM_OPTIONnVALUE	=$this->opt1.$this->opt3.$this->opt2.$this->opt4.$this->val1.$this->val3.$this->val2.$this->val4;
			$ITEM_ID=$this->mid.$this->sid;
			$item_buffer_M = ($MEM_SIZE==176)? $this->D_mem_176 :$this->D_mem_160;
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_GRADE,90,4); 
			$item_buffer_M = self::updateoffset($item_buffer_M,$this->pX,0,2); 
			$item_buffer_M = self::updateoffset($item_buffer_M,$this->pY,4,2); 
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_ID,16,8); 
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_ELEMENT,94,10);
			$item_buffer_M = self::updateoffset($item_buffer_M,$ITEM_OPTIONnVALUE,104,24);
			$item_buffer_M = self::updateoffset($item_buffer_M,$this->pqty,80,4);
			
			$locker_chunk[$this->tab][]=$item_buffer_M;
			
		switch($this->tab){
				case 1:
					$n_locker_H=self::updateoffset($locker_1_head,self::digitz($locker_1_size+1),16,2);
					$this->_buffer_memory=$locker_version.$n_locker_H.implode($locker_chunk[1]);
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$locker_4_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					
					break;
				case 2:
					$n_locker_H=self::updateoffset($locker_2_head,self::digitz($locker_2_size+1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[2]);
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$locker_4_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					break;
				case 3:
					$n_locker_H=self::updateoffset($locker_3_head,self::digitz($locker_3_size+1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[3]);
					$this->_buffer_memory.=$locker_4_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					break;
				case 4:
					$n_locker_H=self::updateoffset($locker_4_head,self::digitz($locker_4_size+1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[4]);
					$this->_buffer_memory.=$locker_5_head;
					if($locker_5_size>0){$this->_buffer_memory.=implode($locker_chunk[5]);}
					break;
				case 5:
					$n_locker_H=self::updateoffset($locker_5_head,self::digitz($locker_5_size+1),16,2);
					$this->_buffer_memory.=$locker_version.$locker_1_head;
					if($locker_1_size>0){$this->_buffer_memory.=implode($locker_chunk[1]);}
					$this->_buffer_memory.=$locker_2_head;
					if($locker_2_size>0){$this->_buffer_memory.=implode($locker_chunk[2]);}
					$this->_buffer_memory.=$locker_3_head;
					if($locker_3_size>0){$this->_buffer_memory.=implode($locker_chunk[3]);}
					$this->_buffer_memory.=$locker_5_head;
					if($locker_4_size>0){$this->_buffer_memory.=implode($locker_chunk[4]);}
					$this->_buffer_memory.=$n_locker_H.implode($locker_chunk[4]);
					break;
			}
			return true;
	}
	private function SaveMode(){
		$model= $this->requireModel('UserInven',$this->requirement['mssql']);
		$filter="UserNum=$this->UserNum";
        $model->updateByAttribute("UserInven=convert(varbinary(MAX),0x$this->_buffer_memory)",$filter);
       
	}
	public function getMemory($a,$b,$c){
		$b=substr($a,$b,$c);
		return $b;
	}
	public function getData($a,$b,$c){return hexdec(substr($a,$b,$c));}
	public function getOffset($a,$b,$c){$f=substr($a,$b,2)."".substr($a,$c,2);return hexdec($f);}
	public function ENUM($a,$b,$c){return hexdec(substr($a,$b,$c));}
	public function trimID($a,$b,$c){$b=substr($a,$b,$c);$c=str_split($b,2);$d=$c[1].$c[0];return hexdec($d);}
	public function fixID($a){$b=dechex($a);$c=str_pad(strtoupper($b),4,0,STR_PAD_LEFT);$d=str_split($c,2);return $d[1].$d[0];}
	public function trimoffset($a){$b=dechex($a);return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);}
	public function trimOpt($a){$b=dechex($a);$c=str_pad(strtoupper($b),4,0,STR_PAD_LEFT);$d=str_split($c,2);return $d[1].$d[0];}
	public function updateoffset($a,$b,$c,$d){return substr_replace($a,$b,$c,$d);}
	public function digitz($a){$b=dechex($a);return str_pad(strtoupper($b),2,0,STR_PAD_LEFT);
	}
}