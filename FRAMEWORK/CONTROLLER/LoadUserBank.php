<?php
error_reporting(0);
class LoadUserBank extends controller{
    private $UserNum;
	private $data=array();
	
	private $_username;
    private $requirement;
    function __construct($requirement,$data=null) {
            $this->UserNum=$data['UserNum'];
            $this->requirement=$requirement;
			self::fetchUserData();
			self::fetchStore();
			self::viewUserBank();
			self::pushJSON($this->data); 
            
    }
	private function fetchUserData(){
		 $model= $this->requireModel('UserInfo',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="UserID";
    
            //add filters//
            $filter="UserNum=$this->UserNum";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
				self::errorCODE(1);
				exit();
			}else{
				$this->_username = $result[0]['UserID'];
				
			}
	}
    private function fetchStore(){
          
            $model= $this->requireModel('ShopItemMap',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ItemMain,ItemSub,ItemName,ProductNum,ItemName";
    
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col);
			if($result==null){
                self::errorCODE(1); 
            }else{
				
				$this->data['store']=$result; 
               
            }
           
    }
	private function viewUserBank(){
		
            $model= $this->requireModel('ShopPurchase',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="PurKey,ProductNum,PurPrice";
			$filter="UserUID='$this->_username' AND PurFlag=0";
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                $this->data['bank']['key']=""; 
                $this->data['bank']['ProductNum']=""; 
                $this->data['bank']['PurPrice']=""; 
                $this->data['bank']['ItemName']=""; 
            }else{
				
				foreach($result as $test){
					$this->data['bank']['key'][]=$test['PurKey'];
					$this->data['bank']['ProductNum'][]=$test['ProductNum'];
					$this->data['bank']['PurPrice'][]=$test['PurPrice'];
					$this->data['bank']['ItemName'][]=self::getItemName($test['ProductNum']);
				}
               
            }
	}
	private function getItemName($a){

			$model= $this->requireModel('ShopItemMap',$this->requirement['mssql']);
           
            //column to fetch//
            $col ="ItemName";
			$filter="ProductNum=$a";
            //get data from model//you can leave argument empty;//
            $result=$model->findByAttribute($col,$filter);
			if($result==null){
                return "NO IDEA";
            }else{
				
				return $result[0]['ItemName']; 
               
			}
	}
	
}