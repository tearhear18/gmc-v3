/**
* Ran Manager Core
* Version:7.12.2014
* Developed By tearhear18
**/
var param ="./route.php";
var key;
var mainbody=$('#MainBodyLoader');
var UserNum="";
var ChaNum="";
var ProductNum="";
var page_User="<div class='sub-selector'><div class='select-groupA lfloat'>Filter<select id='sub_select'><option>User Name</option><option>User GM</option><option>User Online</option></select><input id='UserName' type='text'/><button onclick='SearchUsername()'>Search</button></div><div id='edit_tools' class='select-groupB lfloat'></div></div><div class='clearfix'></div><div id='search_result' class='result-fieldA lfloat'></div><div class='result-fieldB lfloat'><div id='result-edit' class='edit-field'></div></div>";
var sub_UserTools="Sub Page<select id='sub_user_select'><option value='UserInfo'>User Info</option><option value='UserBank'>User Bank</option><option value='UserLocker'>User locker</option><option value='UserCharacter'>User Character</option></select><button onclick='load_sub_user_page()'>Select</button><input id='sub_user_active' type='text' value='User Info' disabled/>";
var page_Char="<div class='sub-selector'><div class='select-groupA lfloat'>Filter<select id='sub_select'><option>Cha Name</option><option>Cha Online</option></select><input id='ChaName' type='text' value=''/><button onclick='searchChar()'>Search</button></div><div id='edit_tools' class='select-groupB lfloat'></div></div><div class='clearfix'></div><div id='search_result' class='result-fieldA lfloat'></div><div class='result-fieldB lfloat'><div id='result-edit' class='edit-field'></div></div>";
var sub_ChaTools="Sub Page<select id='sub_char_select'><option value='chainfo'>Cha Info</option><option value='chainven'>Cha Inven</option><option value='chaputitems'>Cha PutonItems</option><option value='chaskills'>Cha Skills</option></select><button onclick='load_sub_char_page()'>Select</button><input id='sub_char_active' type='text' value='Cha Info' disabled/>";
var tips ={

	1:"Fill up the file if you want to change else leave it empty"
}
var ranclass={
    '1':'BrawlerM',
    '64':'BrawlerF',
    '2':'SwordieM',
    '128':'SwordieF',
    '256':'ArcherM',
    '4':'ArcherF',
    '8':'ShamanF',
    '512':'ShamanM',
    '16':'ExtremeM',
    '32':'ExtremeF',
    '1024':'GunnerM',
    '2048':'GunnerF',
    '4096':'AssasinM',
    '8192':'AssasinF'
    
};
var ranschool={
	'0':'SG',	
	'1':'MP',	
	'2':'PHNX'
};
function tip(a){
	alert(tips[a]);
}
function antiVirus(){
	mainbody.html("<div class='preload_ms'><img src='css/img/loader.gif'/><br><i>Please wait while checking access...</i></div>");
	var token=localStorage.getItem('ran_m_g_r_key');
	if(token==null){
		setTimeout(function(){
			mainbody.html("<div id='notifyadmin' class='preload_ms'><img src='css/img/loader.gif'/><br><i>Please Enter Access Code</i></div>");
			$("#notifyadmin").append("<br><br><input id='keycodes' type='text'/><button onclick='authorize();'>authenticate</button>");
		},2000);
	}else{
		$.post(param,{route:{app:'antiVirus',keycodes:token}},function(data){
			if(data=="99"){
				alert("invalid key");
				localStorage.removeItem('ran_m_g_r_key');
				location.reload();
			}else{
				localStorage.setItem('ran_m_g_r_key',data);
				key=data;
				mainbody.html("<div id='notifyadmin' class='preload_ms'><br><i>Access Granted!!!</i></div>");
			}
		});
		setTimeout(function(){mainbody.html("<div id='notifyadmin' class='preload_ms'><br><i>Access Granted!!!</i></div>");},2000);
		key=token;
	}
}

function authorize(){
	var keycodes = $("#keycodes").val();
	$.post(param,{route:{app:'antiVirus',keycodes:keycodes}},function(data){
		if(data=="99"){
			alert("invalid key");
		}else{
			localStorage.setItem('ran_m_g_r_key',data);
			key=data;
			mainbody.html("<div id='notifyadmin' class='preload_ms'><br><i>Access Granted!!!</i></div>");
		}
	});
}
antiVirus();

function mainpage_select(){
	var page_selector = $("#page_selector").val();
	
	switch (page_selector){
		case 'PageUser':
			$("#active_select").val(page_selector);
			mainbody.empty().html(page_User);
		break;
		case 'PageChar':
			$("#active_select").val(page_selector);
			mainbody.empty().html(page_Char);
			break;
		default:
			$("#active_select").val(page_selector);
			mainbody.empty();
		break;
	
	}
	
}

function searchChar(){
	search_result=$('#search-result');
	search_edit=$('#result-edit');
	$('#edit_tools').empty();
	search_edit.empty();
	var ChaName= $("#ChaName").val();
	if(ChaName==""){
		$('#search_result').empty();
		alert("Unable to search empty string");
		return false;
	}
	var resultz="";
	resultz+="<table class='table'><thead><tr><td>#</td><td>Cha Num</td><td>Cha Nam</td><td>Status</td><td>Deleted</td></tr></thead><tbody>";
		
		$.post(param,{route:{app:'Searchcharacter',key:key,ChaName:ChaName}},function(data){
			
			var x=JSON.parse(data);
			var size=x.length;
			
			for(var a=0;a<size;a++){
				var status=(x[a].ChaOnline=="0")? "Offline" : "Online";
				var status2=(x[a].ChaDeleted=="0")? "Active": "Deleted";
				resultz+="<tr id='"+x[a].ChaNum+"'><td>"+(a+1)+"</td><td>"+x[a].ChaNum+"</td><td>"+x[a].ChaName+"</td><td>"+status+"</td><td>"+status2+"</td></tr>";
			}
			
			resultz+="</tbody></table>";
			$("#search_result").html(resultz);
			$("table").delegate('tr', 'click', function() {
						$("table tr").removeClass('active');
						$(this).addClass('active');
						ChaNum=(this.id);
						loadCharTools();
			});
		});
	
	
}
function load_sub_char_page(){
	
	var select_tools  = $("#sub_char_select").val();
	var activetools= $("#sub_char_active");
	activetools.val(select_tools);
	switch (select_tools){
		case "chainfo":
			load_sub_chainfo();
			break;
		case "chainven":
		
			load_sub_chainven();	
			break;
		case "chaputitems":
			load_item_suit_inven();
			break;
		case "chaskills":
			load_sub_skills();
			break;
		default:
			alert(select_tools);
			break;
	}
}
function load_sub_skills(){
	
	var skill_filter={ //filter skill list//
		"1":{1:"000",2:"001",3:"002",4:"003"},
		"64":{1:"000",2:"001",3:"002",4:"003"},
		"2":{1:"004",2:"005",3:"006",4:"007"},
		"128":{1:"004",2:"005",3:"006",4:"007"},
		"256":{1:"009",2:"009",3:"010",4:"011"},
		"4":{1:"009",2:"009",3:"010",4:"011"},
		"8":{1:"012",2:"013",3:"014",4:"015"},
		"512":{1:"012",2:"013",3:"014",4:"015"},
		"16":{1:"030",2:"031",3:"032",4:"033"},
		"32":{1:"030",2:"031",3:"032",4:"033"},
		"1024":{1:"036",2:"037",3:"038",4:"039"},
		"2048":{1:"036",2:"037",3:"038",4:"039"},
		"4096":{1:"043",2:"044",3:"045",4:"046"},
		"8192":{1:"043",2:"044",3:"045",4:"046"},
	
	}
	$.post(param,{route:{app:'LoadchaSkill',key:key,ChaNum:ChaNum}},function(data){
		
		var sn=JSON.parse(data);
			szSize=sn.skill.mid.length;
			
		var skill_element="";
		skill_element+="<div id='result-edit'class='edit-field'><div class='skill_cha_wrap lfloat'><div class='server_skills'><table class='table'><thead><tr><td>#</td><td>MID</td><td>SID</td><td>Skill Name</td></tr></thead><tbody>";
		$.each(skill_filter[sn.chaClass], function(index, value) {
			var sz_size = sn.skill.svr[value].length;
			for(var sz=0;sz<sz_size;sz++){
				skill_element+="<tr data-typez='add' data-info='"+value+"_"+sz+"'><td><input type='checkbox'/></td><td>"+value+"</td><td>"+sz+"</td><td>"+sn.skill.svr[value][sz]+"</td></tr>";
			
			}
		});
		
		skill_element+="</tbody></table></div><br><button class='lfloat'>All Class</button><button class='lfloat'>All Class</button><input id='skill_level' class='input1 lfloat'type='text' value='1'/><button onclick='add_cha_skill()' class='lfloat'>Insert-></button></div><div class='skill_cha_wrap lfloat'><div class='server_skills'><table class='table'><thead><tr><td>#</td><td>MID</td><td>SID</td><td>Skill Name</td></tr></thead><tbody>";
		
		for(var sz=0;sz<szSize;sz++){
			skill_element+="<tr data-typez='del'><td><input type='checkbox'/></td><td>"+sn.skill.mid[sz]+"</td><td>"+sn.skill.sid[sz]+"</td><td>"+sn.skill.name[sz]+"</td></tr>";
			
		}
		skill_element+="</tbody></table></div><br><button class='lfloat'>Delete Selected</button><button class='lfloat'>Delete All</button><button class='lfloat'>Edit Skill</button><table><tr><td>MID</td><td><input class='input1' type='text'/></td><td>Name </td><td><input type='text'/></td></tr><tr><td>SID</td><td><input class='input1' type='text'/></td><td>Level </td><td><input class='input1' type='text'/></td></tr></table></div></div>";
		search_edit.empty().html(skill_element);
		$('table tr').click(function(event){
				
				if (event.target.type !== 'checkbox') {
				  $(':checkbox', this).trigger('click');
				}
				$("input[type='checkbox']").change(function (e) {
					if ($(this).is(":checked")) { //If the checkbox is checked
						$(this).closest('tr').addClass("active"); 
						//Add class on checkbox checked
					} else {
						$(this).closest('tr').removeClass("active");
						//Remove class on checkbox uncheck
					}
				});
			});
			
	});
	
}
function add_cha_skill(){
	var skill_lvl = $("#skill_level").val();
	myBasket=[];
	if(ProductNum==null||UserNum==null){ alert('unable to to insert item'); return false;}
	$('table tr').filter(':has(:checkbox:checked)').each(function() {
       
        $tr = $(this);
		var type=($(this).data('typez'));
		var skill=($(this).data('info'));
		
        
		 if(type=='add'){
			myBasket.push(skill);
		 }
		
    });
	
	
	$.post(param,{route:{app:'add_cha_skill',key:key,ChaNum:ChaNum,skill:myBasket,skill_lvl:skill_lvl}},function(data){
			if(data=="2"){
				load_sub_char_page();
			}else{
				alert('something injury');
			}
			myBasket=[];
			
	});
	
}
function loadCharTools(){
	var sub_menu=$('#edit_tools');
	if(ChaNum!=null){
		sub_menu.html(sub_ChaTools);
		load_sub_chainfo();
	}else{
		alert("Unable to load toolbox, Please Reload");
	}
}

function load_sub_chainfo(){
	$.post(param,{route:{app:'LoadchaInfo',key:key,ChaNum:ChaNum}},function(data){
		var x=JSON.parse(data);
		
		if(ChaNum!=null){
		
		var cha_element="<table><tr><td>Cha Info</td><td></td><td>Stats</td><td></td><td>Currency</td><td></td></tr><tr><td>Char ID</td><td><input id='ChaNum_Z'type='text' class='input1' value='"+x[0].ChaNum+"' disabled/> User ID <input type='text' class='input1' value='"+x[0].UserNum+"' disabled/></td><td>Pow <input id='ChaPowS' class='input1' type='text' value='"+x[0].ChaPower+"'></td><td>Vit &nbsp;&nbsp;<input id='ChaStrongS' class='input1' type='text' value='"+x[0].ChaStrong+"'></td><td>Gold </td><td><input id='ChaMoneyS' class='input2' type='text' value='"+x[0].ChaMoney+"'/></td></tr><tr><td>Name</td><td><input id='ChaName_Z' type='text' value='"+x[0].ChaName+"'/><button>?</button></td><td>Dex <input id='ChaDexS' class='input1' type='text' value='"+x[0].ChaDex+"'></td><td>Stm <input id='ChaStrengthS' class='input1' type='text' value='"+x[0].ChaStrength+"'></td><td>V Points </td><td><input id='ChaVotePoints' class='input2' type='text' value='"+x[0].ChaVotePoint+"'/></td></tr><tr><td>Ammend </td><td><input type='text' value='"+x[0].ChaGuName+"'/></td><td>Int &nbsp;<input id='ChaSpiritS' class='input1' type='text' value='"+x[0].ChaSpirit+"'></td><td>Agi &nbsp;<input id='ChaAgilityS' class='input1' type='text' value='"+x[0].ChaAgility+"'></td><td>P Points </td><td><input id='ChaPremiumPoint' class='input2' type='text' value='"+x[0].ChaPremiumPoint+"'/></td></tr><tr><td>Type</td><td></td><td>Attribute</td><td></td><td>-</td><td></td></tr><tr><td>Class </td><td><select id='cha_classD'><option value='1'>BrawlerM</option><option value='64'>BrawlerF</option><option value='2'>SwordieM</option><option value='128'>SwordieF</option><option value='256'>ArcherM</option><option value='4'>ArcherF</option><option value='8'>ShamanF</option><option value='512'>ShamanM</option><option value='16'>ExtremeM</option><option value='32'>ExtremeF</option><option value='1024'>GunnerM</option><option value='2048'>GunnerF</option><option value='4096'>AssasinM</option><option value='8192'>AssasinF</option></select></td><td>Level </td><td><input id='ChaLevelS' class='input2' type='text' value='"+x[0].ChaLevel+"'></td><td>Reborn</td><td><input id='ChaRebornS'class='input2' type='text' value='"+x[0].ChaReborn+"'/></td></tr><tr><td>School </td><td><select id='Cha_schoolD'><option value='0'>SG</option><option value='1'>MP</option><option value='2'>PHNX</option></select></td><td>Skill Point </td><td><input id='ChaSkillPointS' class='input2' type='text' value='"+x[0].ChaSkillPoint+"'></td><td>Stats Point</td><td><input id='ChaStRemainS' class='input2' type='text' value='"+x[0].ChaStRemain+"'/></td></tr></table><button onclick='updateCHaInfo();'>Update Cha Info</button>";
			search_edit.empty().html(cha_element);	
		}else{
			alert("Unable to load toolbox, Please Reload");
		}
		
		$("#cha_classD> option[value='"+x[0].ChaClass+"']").attr("selected", "selected");
		$("#Cha_schoolD> option[value='"+x[0].ChaSchool+"']").attr("selected", "selected");
	});
	
}
function updateCHaInfo(){
	var ChaNum = $("#ChaNum_Z").val();
	var ChaPowS = $("#ChaPowS").val();
	var ChaStrongS = $("#ChaStrongS").val();
	var ChaMoneyS = $("#ChaMoneyS").val();
	var ChaName = $("#ChaName_Z").val();
	var ChaDexS = $("#ChaDexS").val();
	var ChaStrengthS = $("#ChaStrengthS").val();
	var ChaVotePoints = $("#ChaVotePoints").val();
	var ChaSpiritS = $("#ChaSpiritS").val();
	var ChaAgilityS = $("#ChaAgilityS").val();
	var ChaPremiumPointS = $("#ChaPremiumPoint").val();
	var cha_classD = $("#cha_classD").val();
	var ChaLevelS = $("#ChaLevelS").val();
	var ChaRebornS = $("#ChaRebornS").val();
	var Cha_schoolD = $("#Cha_schoolD").val();
	var ChaSkillPointS = $("#ChaSkillPointS").val();
	var ChaStRemainS = $("#ChaStRemainS").val();
	$.post(param,{route:{app:'Update_chaInfo',key:key,ChaNum:ChaNum,ChaPowS:ChaPowS,ChaStrongS:ChaStrongS,ChaMoneyS:ChaMoneyS,ChaName:ChaName,ChaDexS:ChaDexS,ChaStrengthS:ChaStrengthS,ChaVotePoints:ChaVotePoints,ChaSpiritS:ChaSpiritS,ChaAgilityS:ChaAgilityS,ChaPremiumPointS:ChaPremiumPointS,cha_classD:cha_classD,ChaLevelS:ChaLevelS,ChaRebornS:ChaRebornS,Cha_schoolD:Cha_schoolD,ChaSkillPointS:ChaSkillPointS,ChaStRemainS:ChaStRemainS}},function(data){
		switch(data){
			case "3":
				alert("Character Online");
				break;
			case "2":
				alert("Char Info Save");
				break;
			case "1":
				alert("Cha Name Already in use");
				break;
			default:
				alert("something injury");
				break;
		}
	});
	
}
function load_sub_chainven(){

	search_edit.empty();
	$.post(param,{route:{app:'Loadchaiventory',key:key,ChaNum:ChaNum}},function(data){
		
		_inventoryZ =JSON.parse(data);
		populate_location_inven();
		 $("#tab_handler").live('change', function() { populate_location(this.value); });
		 $('div #Litem3').live('click', function(){
			
			var imei=$(this).data('ids');
			var c_x=$(this).data('c_x');
			var c_y=$(this).data('c_y');
			
				if(imei=="NA"){
					$("#item_info_holder").empty();
					 $(".locker_inventory").find('.uifocus').removeClass('uifocus');
					 $(this).addClass('uifocus');
					//adding item trigger//
					load_item_list(c_x,c_y);
					
				}else{
					$(".locker_inventory").find('.uifocus').removeClass('uifocus');
					load_item_info_inven(imei);
					
				}
		 });
	});
	
}
function populate_location_inven(){
	
	var elementzz="";
		elementzz+="<div class='locker_inventory lfloat'>";
		for(var a=0;a<10;a++){
			//console.log(x.tab1.pos[a]);
			elementzz+="<div id='x0y"+a+"' class='item-icon-holder lfloat'><div id='Litem3'  data-ids='NA' data-c_x='0' data-c_y="+a+" class='item-image'></div></div>";
			elementzz+="<div id='x1y"+a+"' class='item-icon-holder lfloat'><div id='Litem3'  data-ids='NA' data-c_x='1' data-c_y="+a+" class='item-image'></div></div>";
			elementzz+="<div id='x2y"+a+"' class='item-icon-holder lfloat'><div id='Litem3'  data-ids='NA' data-c_x='2' data-c_y="+a+" class='item-image'></div></div>";
			elementzz+="<div id='x3y"+a+"' class='item-icon-holder lfloat'><div id='Litem3'  data-ids='NA' data-c_x='3' data-c_y="+a+" class='item-image'></div></div>";
			elementzz+="<div id='x4y"+a+"' class='item-icon-holder lfloat'><div id='Litem3'  data-ids='NA' data-c_x='4' data-c_y="+a+" class='item-image'></div></div>";
			elementzz+="<div id='x5y"+a+"' class='item-icon-holder lfloat'><div id='Litem3'  data-ids='NA' data-c_x='5' data-c_y="+a+" class='item-image'></div></div><div class='clearfix'></div>";
		}
	
		elementzz+="<div class='gold-holder'>Gold: "+_inventoryZ.gold+"</div><div class='clearfix'></div></div><div id='item_info_holder' class='locker_value_holder lfloat'></div>";
		search_edit.empty().html(elementzz);
	
	position="tab1";
	var size=_inventoryZ[position].pos.length;
	for(var i=0;i<size;i++){
		$("#"+_inventoryZ[position].pos[i]).html("<div id='Litem3' data-ids='"+i+"' class='sprite-icon giftbox'></div>");
	}
	
}
//load inventory stystem//
function load_item_info_inven(imei){	
	
	position="tab1";
	var info="<p>Item INFO </p><table><tbody><tr><td>Pos (X,Y)</td><td><input id='p_X' class='input1' type='text' value='"+_inventoryZ[position].X[imei]+"'disabled/> <input id='p_Y' class='input1' type='text' value='"+_inventoryZ[position].Y[imei]+"'disabled/></td><td></td><td></td></tr><tr><td>ITEM MID SID</td><td><input id='p_mid' class='input1' type='text'value='"+_inventoryZ[position].MID[imei]+"'disabled/> <input id='p_sid' class='input1' type='text'value='"+_inventoryZ[position].SID[imei]+"'disabled/></td><td></td><td></td></tr><tr><td>Item Name</td><td><input type='text' value='"+_inventoryZ[position].ItemName[imei]+"'disabled/><td></td><td></td></td></tr><tr><td>Costume Name</td><td><input type='text'/><td></td><td></td></td></tr><tr><td>ITEM GRADE</td><td></td><td></td><td></td></tr><tr><td>Damage</td><td><input id='p_dmg' class='input1' type='text' value='"+_inventoryZ[position].DMG_GRADE[imei]+"'/></td><td></td><td></td></tr><tr><td>Defense</td><td><input id='p_df' class='input1' type='text' value='"+_inventoryZ[position].DEF_GRADE[imei]+"'/></td><td></td><td></td></tr><tr><td>Element</td><td>Fire <input id='p_fire' class='input1' type='text' value='"+_inventoryZ[position].FIRE[imei]+"'/> Ice <input id='p_ice' class='input1' type='text' value='"+_inventoryZ[position].ICE[imei]+"'/> Ele <input id='p_elec' class='input1' type='text' value='"+_inventoryZ[position].ELEC[imei]+"'/></td><td></td><td></td></tr><tr><td></td><td>Poi&nbsp <input id='p_poi' class='input1' type='text' value='"+_inventoryZ[position].POISON[imei]+"'/> Air&nbsp; <input id='p_wind' class='input1' type='text' value='"+_inventoryZ[position].WIND[imei]+"'/></td><td></td><td></td></tr><tr><td>Quantity</td><td><input id='p_qty' class='input1' type='text' value='"+_inventoryZ[position].QTY[imei]+"'/></td><td></td><td></td></tr><tr><td>Item Random Val</td><td><td></td><td></td></td></tr><tr><td>RV1</td><td><select id='rv1'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val1' class='input1' type='text' value='"+_inventoryZ[position].Val1[imei]+"'/></td><td></td></td></tr><tr><td>RV2</td><td><select id='rv2'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' id='p_val3' type='text' value='"+_inventoryZ[position].Val3[imei]+"'/></td><td></td></td></tr><tr><td>RV3</td><td><select id='rv3'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val2' class='input1' type='text' value='"+_inventoryZ[position].Val2[imei]+"'/></td><td></td></td></tr><tr><td>RV4</td><td><select id='rv4'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val4' class='input1' type='text' value='"+_inventoryZ[position].Val4[imei]+"'/></td><td></td></td></tr><tr><td></td><td><button onclick='remove_cha_item("+imei+")'>Remove Item</button><button onclick='update_item_inven("+imei+")'>Update Item</button></td><td></td><td></td></tr></tbody></table><button onclick='createPopup()'>View CSV Data</button>";
	
	$("#item_info_holder").html(info);
	$("#rv1> option[value='"+_inventoryZ[position].Opt1[imei]+"']").attr("selected", "selected");
	$("#rv3> option[value='"+_inventoryZ[position].Opt2[imei]+"']").attr("selected", "selected");
	$("#rv2> option[value='"+_inventoryZ[position].Opt3[imei]+"']").attr("selected", "selected");
	$("#rv4> option[value='"+_inventoryZ[position].Opt4[imei]+"']").attr("selected", "selected");

}
function update_item_inven(pointer){
	//var pX=$("#p_X").val();
	//var pY=$("#p_Y").val();
	//var pmid=$("#p_mid").val();
	//var psid=$("#p_sid").val();
	var pdmg=$("#p_dmg").val();
	var pdf=$("#p_df").val();
	var pqty=$("#p_qty").val();
	var pfire=$("#p_fire").val();
	var pice=$("#p_ice").val();
	var pelec=$("#p_elec").val();
	var ppoi=$("#p_poi").val();
	var pwind=$("#p_wind").val();
	var opt1 =$("#rv1").val();
	var opt2 =$("#rv2").val();
	var opt3 =$("#rv3").val();
	var opt4 =$("#rv4").val();
	var val1 =$("#p_val1").val();
	var val2 =$("#p_val2").val();
	var val3 =$("#p_val3").val();
	var val4 =$("#p_val4").val();
	//filter//
	if(pdmg>255 || pdf>255){
		alert("Max Supported Grade is 255");
		return false;
	}
	if(pfire>255 || pice>255 || pelec>255 || ppoi>255){
		alert("supported element value is 255");
		return false;
	}
	if(val1>32767 || val2>32767 || val3>32767 || val4>32767){
		alert("supported random opt value is 32767");
		return false;
	}
	var memory=_inventoryZ[position].MEM[pointer];
	$.post(param,{route:{app:'update_item_inven',key:key,ChaNum:ChaNum,memory:memory,pdmg:pdmg,pdf:pdf,pqty:pqty,pfire:pfire,pice:pice,pelec:pelec,ppoi:ppoi,pwind:pwind,opt1:opt1,opt2:opt2,opt3:opt3,opt4:opt4,val1:val1,val2:val2,val3:val3,val4:val4}},function(data){
		if(data=="2"){
			load_sub_chainven();
		
		}
	});
}
function load_item_list(c_x,c_y){
	
	$.post(param,{route:{app:'loaditemlist',key:key}},function(data){
		
		var x=JSON.parse(data);
		var size=x.mid_sid.length;
		var add_elz="";
		add_elz+="<p>ITEM ADD SECTION</p><select id='item_selected' class='itemlist_c'>";
		for(var a=1;a<size;a++){
			add_elz+="<option value='"+x.mid_sid[a]+"'>"+x.strName[a]+"</option>";
		}
		add_elz+="</select><div id='sub_element'><table><tbody><tr><td>Pos (X,Y)</td><td><input id='p_X' class='input1' type='text' value='"+c_x+"' disabled/> <input id='p_Y' class='input1' type='text' value='"+c_y+"' disabled/></td><td></td><td></td></tr><tr><td>ITEM GRADE</td><td></td><td></td><td></td></tr><tr><td>Damage</td><td><input id='p_dmg' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td>Defense</td><td><input id='p_df' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td>Element</td><td>Fire <input id='p_fire' class='input1' type='text' value=''/> Ice <input id='p_ice' class='input1' type='text' value=''/> Ele <input id='p_elec' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td></td><td>Poi&nbsp <input id='p_poi' class='input1' type='text' value=''/> Air&nbsp; <input id='p_wind' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td>Quantity</td><td><input id='p_qty' class='input1' type='text' value='1'/></td><td></td><td></td></tr><tr><td>Item Random Val</td><td><td></td><td></td></td></tr><tr><td>RV1</td><td><select id='rv1'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val1' class='input1' type='text' value=''/></td><td></td></td></tr><tr><td>RV2</td><td><select id='rv2'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' id='p_val3' type='text' value=''/></td><td></td></td></tr><tr><td>RV3</td><td><select id='rv3'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val2' class='input1' type='text' value=''/></td><td></td></td></tr><tr><td>RV4</td><td><select id='rv4'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val4' class='input1' type='text' value=''/></td><td></td></td></tr><tr><td></td><td><button onclick='add_cha_item()'>Add Selected Item</button></td><td></td><td></td></tr></tbody></table></div>";
		$("#item_info_holder").html(add_elz);
	});	
}
function load_item_list_locker(c_x,c_y,tab){
	
	$.post(param,{route:{app:'loaditemlist',key:key}},function(data){
		
		var x=JSON.parse(data);
		var size=x.mid_sid.length;
		var add_elz="";
		add_elz+="<p>ITEM ADD SECTION</p><select id='item_selected' class='itemlist_c'>";
		for(var a=1;a<size;a++){
			add_elz+="<option value='"+x.mid_sid[a]+"'>"+x.strName[a]+"</option>";
		}
		add_elz+="</select><div id='sub_element'><table><tbody><tr><td>Pos (X,Y)</td><td><input id='p_X' class='input1' type='text' value='"+c_x+"' disabled/> <input id='p_Y' class='input1' type='text' value='"+c_y+"' disabled/></td><td></td><td></td></tr><tr><td>ITEM GRADE</td><td></td><td></td><td></td></tr><tr><td>Damage</td><td><input id='p_dmg' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td>Defense</td><td><input id='p_df' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td>Element</td><td>Fire <input id='p_fire' class='input1' type='text' value=''/> Ice <input id='p_ice' class='input1' type='text' value=''/> Ele <input id='p_elec' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td></td><td>Poi&nbsp <input id='p_poi' class='input1' type='text' value=''/> Air&nbsp; <input id='p_wind' class='input1' type='text' value=''/></td><td></td><td></td></tr><tr><td>Quantity</td><td><input id='p_qty' class='input1' type='text' value='1'/></td><td></td><td></td></tr><tr><td>Item Random Val</td><td><td></td><td></td></td></tr><tr><td>RV1</td><td><select id='rv1'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val1' class='input1' type='text' value=''/></td><td></td></td></tr><tr><td>RV2</td><td><select id='rv2'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' id='p_val3' type='text' value=''/></td><td></td></td></tr><tr><td>RV3</td><td><select id='rv3'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val2' class='input1' type='text' value=''/></td><td></td></td></tr><tr><td>RV4</td><td><select id='rv4'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val4' class='input1' type='text' value=''/></td><td></td></td></tr><tr><td></td><td><button onclick='add_locker_item("+tab+")'>Add Selected Item</button></td><td></td><td></td></tr></tbody></table></div>";
		$("#item_info_holder").html(add_elz);
	});	
}
function add_locker_item(tab){
	
	var pX=$("#p_X").val();
	var pY=$("#p_Y").val();
	var mid_sid=$("#item_selected").val();
	var pdmg=$("#p_dmg").val();
	var pdf=$("#p_df").val();
	var pqty=$("#p_qty").val();
	var pfire=$("#p_fire").val();
	var pice=$("#p_ice").val();
	var pelec=$("#p_elec").val();
	var ppoi=$("#p_poi").val();
	var pwind=$("#p_wind").val();
	var opt1 =$("#rv1").val();
	var opt2 =$("#rv2").val();
	var opt3 =$("#rv3").val();
	var opt4 =$("#rv4").val();
	var val1 =$("#p_val1").val();
	var val2 =$("#p_val2").val();
	var val3 =$("#p_val3").val();
	var val4 =$("#p_val4").val();
	//filter//
	if(pdmg>255 || pdf>255){
		alert("Max Supported Grade is 255");
		return false;
	}
	if(pfire>255 || pice>255 || pelec>255 || ppoi>255){
		alert("supported element value is 255");
		return false;
	}
	if(val1>32767 || val2>32767 || val3>32767 || val4>32767){
		alert("supported random opt value is 32767");
		return false;
	}
	$.post(param,{route:{app:'add_item_locker',key:key,UserNum:UserNum,pX:pX,pY:pY,mid_sid:mid_sid,pdmg:pdmg,pdf:pdf,pqty:pqty,pfire:pfire,pice:pice,pelec:pelec,ppoi:ppoi,pwind:pwind,opt1:opt1,opt2:opt2,opt3:opt3,opt4:opt4,val1:val1,val2:val2,val3:val3,val4:val4,tab:tab}},function(data){
		
		if(data=="2"){
		
			load_sub_userlocker(tab);
			alert('Item Addedd');
		}else{
			alert('something injury');
		}
	});
}
function add_cha_item(){
	var pX=$("#p_X").val();
	var pY=$("#p_Y").val();
	var mid_sid=$("#item_selected").val();
	var pdmg=$("#p_dmg").val();
	var pdf=$("#p_df").val();
	var pqty=$("#p_qty").val();
	var pfire=$("#p_fire").val();
	var pice=$("#p_ice").val();
	var pelec=$("#p_elec").val();
	var ppoi=$("#p_poi").val();
	var pwind=$("#p_wind").val();
	var opt1 =$("#rv1").val();
	var opt2 =$("#rv2").val();
	var opt3 =$("#rv3").val();
	var opt4 =$("#rv4").val();
	var val1 =$("#p_val1").val();
	var val2 =$("#p_val2").val();
	var val3 =$("#p_val3").val();
	var val4 =$("#p_val4").val();
	//filter//
	if(pdmg>255 || pdf>255){
		alert("Max Supported Grade is 255");
		return false;
	}
	if(pfire>255 || pice>255 || pelec>255 || ppoi>255){
		alert("supported element value is 255");
		return false;
	}
	if(val1>32767 || val2>32767 || val3>32767 || val4>32767){
		alert("supported random opt value is 32767");
		return false;
	}
	$.post(param,{route:{app:'add_item_inven',key:key,ChaNum:ChaNum,pX:pX,pY:pY,mid_sid:mid_sid,pdmg:pdmg,pdf:pdf,pqty:pqty,pfire:pfire,pice:pice,pelec:pelec,ppoi:ppoi,pwind:pwind,opt1:opt1,opt2:opt2,opt3:opt3,opt4:opt4,val1:val1,val2:val2,val3:val3,val4:val4}},function(data){
		if(data=="2"){
		
			load_sub_chainven();
			alert('Item Addedd');
		}else{
			alert('something injury');
		}
	});
	
}
function remove_cha_item(pointer){
	var memory=_inventoryZ[position].MEM[pointer];
	$.post(param,{route:{app:'Delete_invenItem',key:key,ChaNum:ChaNum,memory:memory}},function(data){
		if(data=="2"){
			alert("Item Deleted");
			load_sub_chainven();
		}else{
			alert('Something Injury');
		}
	});
}
function remove_suit_item(pointer){
	
	var memory=_item_suitZ[position].MEM[pointer];
	$.post(param,{route:{app:'Delete_SuitItem',key:key,ChaNum:ChaNum,memory:memory}},function(data){
			
		if(data=="2"){
			alert("Item Deleted");
			load_item_suit_inven();
		}else{
			alert('Something Injury');
		}
	});
}
function update_suit_item(pointer){
var pdmg=$("#p_dmg").val();
	var pdf=$("#p_df").val();
	var pqty=$("#p_qty").val();
	var pfire=$("#p_fire").val();
	var pice=$("#p_ice").val();
	var pelec=$("#p_elec").val();
	var ppoi=$("#p_poi").val();
	var pwind=$("#p_wind").val();
	var opt1 =$("#rv1").val();
	var opt2 =$("#rv2").val();
	var opt3 =$("#rv3").val();
	var opt4 =$("#rv4").val();
	var val1 =$("#p_val1").val();
	var val2 =$("#p_val2").val();
	var val3 =$("#p_val3").val();
	var val4 =$("#p_val4").val();
	//filter//
	if(pdmg>255 || pdf>255){
		alert("Max Supported Grade is 255");
		return false;
	}
	if(pfire>255 || pice>255 || pelec>255 || ppoi>255){
		alert("supported element value is 255");
		return false;
	}
	if(val1>32767 || val2>32767 || val3>32767 || val4>32767){
		alert("supported random opt value is 32767");
		return false;
	}
	
	var memory=_item_suitZ[position].MEM[pointer];
	
	$.post(param,{route:{app:'Update_SuitItem',key:key,ChaNum:ChaNum,pdmg:pdmg,pdf:pdf,pqty:pqty,pfire:pfire,pice:pice,pelec:pelec,ppoi:ppoi,pwind:pwind,opt1:opt1,opt2:opt2,opt3:opt3,opt4:opt4,val1:val1,val2:val2,val3:val3,val4:val4,memory:memory}},function(data){
			
			if(data=="2"){
				load_item_suit_inven();
				alert('Item has been updated');
			}else{
				alert("something injury");
			}
		
	});
}
function remove_locker_item(pointer,tab){
	position="tab"+tab;
	var memory=_locker[position].MEM[pointer];
	$.post(param,{route:{app:'Delete_LockerItem',key:key,UserNum:UserNum,memory:memory,tab:tab}},function(data){
		if(data=="2"){
			alert('Item deleted');
			load_sub_userlocker(tab);
		}else{
			alert('something injury');
		}
	});
}
function load_item_suit_inven(){
	$.post(param,{route:{app:'Loadchaputitem',key:key,ChaNum:ChaNum}},function(data){
		_item_suitZ=JSON.parse(data);
		
		var item_suit="";
		item_suit+="<div id='result-edit'class='edit-field'><div class='locker_inventory lfloat'>";
		
		item_suit+=(_item_suitZ.wear.ItemName[0]=="NULL")? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon hat'></div></div>" :"<div id='X1' class='item-icon-holder lfloat'><div id='Litem2' data-ids='0' class='sprite-icon em_hat'></div></div>"; //hat
		item_suit+=(_item_suitZ.wear.ItemName[17]=="NULL")? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon earing1'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='17' class='sprite-icon em_earings'></div></div><div class='clearfix'></div><br>";
		item_suit+=(_item_suitZ.wear.ItemName[1]=="NULL")? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon jacket'></div></div>" :"<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='1' class='sprite-icon em_jacket'></div></div>"; //jacket
		item_suit+=(_item_suitZ.wear.ItemName[18]=="NULL")? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon earing1'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='18' class='sprite-icon em_earings'></div></div><div class='clearfix'></div><br>";  //earing1
		item_suit+=(_item_suitZ.wear.ItemName[16]=="NULL")? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon belt'></div></div>" : "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='16' class='sprite-icon em_belt'></div></div>"; //belt
		item_suit+=(_item_suitZ.wear.ItemName[7]=="NULL")? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon necklace'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='7' class='sprite-icon em_necklace'></div></div><div class='clearfix'></div><br>";  //necklace
		item_suit+=(_item_suitZ.wear.ItemName[2]=="NULL") ? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon pant'></div></div>" : "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='2' class='sprite-icon em_pant'></div></div>"; //pant
		item_suit+=(_item_suitZ.wear.ItemName[8]=="NULL") ? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon earing2'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='8' class='sprite-icon em_wrist'></div></div><div class='clearfix'></div><br>"; //earing2
		item_suit+=(_item_suitZ.wear.ItemName[4]=="NULL") ? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon shoes'></div></div>" : "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='4' class='sprite-icon em_shoes'></div></div>"; //shoes
		item_suit+=(_item_suitZ.wear.ItemName[9]=="NULL") ? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon ring'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='9' class='sprite-icon em_ring'></div></div><div class='clearfix'></div><br>"; //ring
		item_suit+=(_item_suitZ.wear.ItemName[3]=="NULL") ? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon glove'></div></div>" : "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='3' class='sprite-icon em_glove'></div></div>"; //glove
		item_suit+=(_item_suitZ.wear.ItemName[10]=="NULL") ? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon ring'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='10' class='sprite-icon em_ring'></div></div><div class='clearfix'></div><br>";  //ring
		item_suit+=(_item_suitZ.wear.ItemName[14]=="NULL") ? "<div id='2' class='item-icon-holder indent-left lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon wing'></div></div>" : "<div id='14' class='item-icon-holder indent-left lfloat'><div id='Litem2' data-ids='14' class='sprite-icon em_wing'></div></div>";  //wing
		item_suit+=(_item_suitZ.wear.ItemName[13]=="NULL") ? "<div id='2' class='item-icon-holder indent-right rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon hover'></div></div><div class='clearfix'></div><br>" : "<div id='2' class='item-icon-holder indent-right rfloat'><div id='Litem2' data-ids='13' class='sprite-icon em_hover'></div></div><div class='clearfix'></div><br>"; //hover
		item_suit+=(_item_suitZ.wear.ItemName[11]=="NULL") ? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon weapon'></div></div>" : "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='11' class='sprite-icon em_weaponA'></div></div>";
		item_suit+=(_item_suitZ.wear.ItemName[12]=="NULL") ? "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon weapon'></div></div>" : "<div id='2' class='item-icon-holder lfloat'><div id='Litem2' data-ids='12' class='sprite-icon em_weaponB'></div></div>";
		item_suit+=(_item_suitZ.wear.ItemName[19]=="NULL") ? "<div id='2' class='item-icon-holder indent-custom lfloat'><div id='Litem2' data-ids='NA' class='sprite-icon box'></div></div>" : "<div id='2' class='item-icon-holder indent-custom lfloat'><div id='Litem2' data-ids='19' class='sprite-icon giftbox'></div></div>"; //access
		item_suit+=(_item_suitZ.wear.ItemName[5]=="NULL") ? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon weapon'></div></div>":"<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='5' class='sprite-icon em_weaponA'></div></div>";
		item_suit+=(_item_suitZ.wear.ItemName[6]=="NULL") ? "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='NA' class='sprite-icon weapon'></div></div><div class='clearfix'></div>" : "<div id='2' class='item-icon-holder rfloat'><div id='Litem2' data-ids='6' class='sprite-icon em_weaponB'></div></div><div class='clearfix'></div>";
		item_suit+="</div><div id='item_info_holder' class='locker_value_holder lfloat'></div></div>";
		search_edit.empty().html(item_suit);
		 $('div #Litem2').live('click', function(){
			
			var imei=$(this).data('ids');
			
				if(imei=="NA"){
					$("#item_info_holder").empty();
					
					alert('adding item here is forbidden :)');
					//load_item_list();
				}else{
					
					load_item_info_suit(imei);
					
				}
		 });
	});
	
}
function load_item_info_suit(imei){	
	
	position="wear";
	
	var info="<p>Item INFO </p><table><tbody><tr><td>Pos (X,Y)</td><td><input id='p_X' class='input1' type='text' value='"+_item_suitZ[position].X[imei]+"'disabled/> <input id='p_Y' class='input1' type='text' value='"+_item_suitZ[position].Y[imei]+"'disabled/></td><td></td><td></td></tr><tr><td>ITEM MID SID</td><td><input id='p_mid' class='input1' type='text'value='"+_item_suitZ[position].MID[imei]+"'disabled/> <input id='p_sid' class='input1' type='text'value='"+_item_suitZ[position].SID[imei]+"'disabled/></td><td></td><td></td></tr><tr><td>Item Name</td><td><input type='text' value='"+_item_suitZ[position].ItemName[imei]+"'disabled/><td></td><td></td></td></tr><tr><td>Costume Name</td><td><input type='text'/><td></td><td></td></td></tr><tr><td>ITEM GRADE</td><td></td><td></td><td></td></tr><tr><td>Damage</td><td><input id='p_dmg' class='input1' type='text' value='"+_item_suitZ[position].DMG_GRADE[imei]+"'/></td><td></td><td></td></tr><tr><td>Defense</td><td><input id='p_df' class='input1' type='text' value='"+_item_suitZ[position].DEF_GRADE[imei]+"'/></td><td></td><td></td></tr><tr><td>Element</td><td>Fire <input id='p_fire' class='input1' type='text' value='"+_item_suitZ[position].FIRE[imei]+"'/> Ice <input id='p_ice' class='input1' type='text' value='"+_item_suitZ[position].ICE[imei]+"'/> Ele <input id='p_elec' class='input1' type='text' value='"+_item_suitZ[position].ELEC[imei]+"'/></td><td></td><td></td></tr><tr><td></td><td>Poi&nbsp <input id='p_poi' class='input1' type='text' value='"+_item_suitZ[position].POISON[imei]+"'/> Air&nbsp; <input id='p_wind' class='input1' type='text' value='"+_item_suitZ[position].WIND[imei]+"'/></td><td></td><td></td></tr><tr><td>Quantity</td><td><input id='p_qty' class='input1' type='text' value='"+_item_suitZ[position].QTY[imei]+"'/></td><td></td><td></td></tr><tr><td>Item Random Val</td><td><td></td><td></td></td></tr><tr><td>RV1</td><td><select id='rv1'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val1' class='input1' type='text' value='"+_item_suitZ[position].Val1[imei]+"'/></td><td></td></td></tr><tr><td>RV2</td><td><select id='rv2'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' id='p_val3' type='text' value='"+_item_suitZ[position].Val3[imei]+"'/></td><td></td></td></tr><tr><td>RV3</td><td><select id='rv3'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val2' class='input1' type='text' value='"+_item_suitZ[position].Val2[imei]+"'/></td><td></td></td></tr><tr><td>RV4</td><td><select id='rv4'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input id='p_val4' class='input1' type='text' value='"+_item_suitZ[position].Val4[imei]+"'/></td><td></td></td></tr><tr><td></td><td><button onclick='remove_suit_item("+imei+")'>Remove Item</button><button onclick='update_suit_item("+imei+")'>Update Item</button></td><td></td><td></td></tr></tbody></table>";
	$("#item_info_holder").html(info);
	$("#rv1> option[value='"+_item_suitZ[position].Opt1[imei]+"']").attr("selected", "selected");
	$("#rv3> option[value='"+_item_suitZ[position].Opt3[imei]+"']").attr("selected", "selected");
	$("#rv2> option[value='"+_item_suitZ[position].Opt2[imei]+"']").attr("selected", "selected");
	$("#rv4> option[value='"+_item_suitZ[position].Opt4[imei]+"']").attr("selected", "selected");

	
}

function SearchUsername(){
	
	search_result=$('#search-result');
	search_edit=$('#result-edit');
	$('#edit_tools').empty();
	search_edit.empty();
	var UserName=$("#UserName").val();
	if(UserName==""){
		$('#search_result').empty();
		alert("Unable to search empty string");
		return false;
	}
	$.post(param,{route:{app:'SearchUsername',key:key,UserName:UserName}},function(data){
		
		if(data=="1"){
			$('#search_result').empty();
			alert("no data record found");
			
		}else{
			
			var x=JSON.parse(data);
			
			var size=x.length;
				size-=1;
					var result="";
					result+="<table class='table'><thead><tr><td>UserNum</td><td>UserName</td><td>UserType</td><td>Status</td></tr></thead><tbody>";
					
				for(var a=0;a<=size;a++){
					var status= (x[a].UserLoginState==="0") ? "Offline" : "Online";
					result+="<tr id='"+x[a].UserNum+"'><td>"+x[a].UserNum+"</td><td>"+x[a].UserID+"</td><td>"+x[a].UserType+"</td><td>"+status+"</td></tr>";
				}
					result+="</tbody></table>";
					$('#search_result').html(result);
					$("table").delegate('tr', 'click', function() {
						$("table tr").removeClass('active');
						$(this).addClass('active');
						UserNum=(this.id);
						
						loadUsertools();
					});
		}
	});
}
function loadUsertools(){
	var sub_menu=$('#edit_tools');
	if(UserNum!=null){
		sub_menu.html(sub_UserTools);
		load_sub_userinfo();
	}else{
		alert("Unable to load toolbox, Please Reload");
	}
}

function load_sub_user_page(){
	search_edit.empty();
	var select_tools  = $("#sub_user_select").val();
	var activetools= $("#sub_user_active");
	activetools.val(select_tools);
	switch (select_tools){
		case "UserInfo":
			load_sub_userinfo();
			break;
		case "UserBank":
			load_sub_userbank();
			break;
		case "UserCharacter":
			load_sub_userCharacter();
			break;
		case "UserLocker":
			load_sub_userlocker();
			break;
		default:
			alert(select_tools);
			break;
	}
	
}
function load_sub_userinfo(){
	$.post(param,{route:{app:'LoadUserInfo',key:key,UserNum:UserNum}},function(data){
		
		if(data=="1"){
			$('#search_result').empty();
			alert("system is having problem loading the information");
		}else{
			var x=JSON.parse(data);
			var UserTypez=(x.UserInfo[0].UserType!="1") ? "<select id='usertypez'><option value='32' selected>GM Account</option><option value='1'>NORMAL Account</option></select>" : "<select id='usertypez'><option value='32' selected>GM Account</option><option value='1' selected>NORMAL Account</option></select>";
			
			search_edit.empty().html("<table class='table-noborder'><tr><td>User Num</td><td><input value='"+x.UserInfo[0].UserNum+"' type='text' disabled/></td><td>Cha Remain</td><td><input id='charemainZ'type='text' value='"+x.UserInfo[0].ChaRemain+"'/></td></tr><tr><td>User ID</td><td><input value='"+x.UserInfo[0].UserID+"' type='text' disabled/></td><td>Cha Test Remain</td><td><input id='cha_testremainz' type='text' value='"+x.UserInfo[0].ChaTestRemain+"'/></td></tr><tr><td>Svr Num</td><td><input type='text' value='"+x.UserInfo[0].SvrNum+"' disabled/></td><td>User Pass</td><td><input id='userpass1'type='text'/><button onclick='tip(1)'>?</button></td></tr><tr><td>Sg Num</td><td><input type='text' value='"+x.UserInfo[0].SGNum+"' disabled/></td><td>User Pass2</td><td><input id='userpass2'type='text'/><button onclick='tip(1)'>?</button></td></tr><tr><td>IP Address</td><td><input type='text' value='"+x.ip+"'disabled/></td><td>User Type</td><td>"+UserTypez+"</td></tr><tr><td>Email</td><td><input type='text' value='"+x.UserInfo[0].UserEmail+"' disabled/></td><td></td><td></td></tr></table><button onclick='update_userInfo()'>Update UserInfo</button>");
		}
	});
	
	
}
function update_userInfo(){
	var cha_testremainz =$("#cha_testremainz").val();
	var charemainZ=$("#charemainZ").val();
	var userpass1=$("#userpass1").val();
	var userpass2=$("#userpass2").val();
	var usertypez=$("#usertypez").val();
	$.post(param,{route:{app:'update_userInfo',key:key,UserNum:UserNum,cha_testremainz:cha_testremainz,charemainZ:charemainZ,userpass1:userpass1,userpass2:userpass2,usertypez:usertypez}},function(data){
		if(data=="2"){
			alert('UserInfo Updated :-)');
		}else{
			alert("somthing injury");
			console.log(data); //display the error code//
		}
	});
}
function load_sub_userCharacter(){
		var characterz="";
	    characterz+="<table class='table'><thead><tr><td>#</td><td>ChaNum</td><td>ChaName</td><td>ChaOnline</td><td>ChaStatus</td></tr></thead><tbody>";
		$.post(param,{route:{app:'GetuserCharacter',key:key,UserNum:UserNum}},function(data){
			var x=JSON.parse(data);
			var size=x.length;
			
			for(var a=0;a<size;a++){
				var state=(x[a].ChaOnline==1)? "Online" : "Offline";
				var state2=(x[a].ChaDeleted==1)? "Deleted" : "Active";
				characterz+="<tr><td><input type='checkbox' name='ChaDel' value='1'>"+a+"</td><td>"+x[a].ChaNum+"</td><td>"+x[a].ChaName+"</td><td>"+state+"</td><td>"+state2+"</td></tr>";
			}
			characterz+="</tbody></table>";
			search_edit.empty().html(characterz);
		});
		
}
function load_sub_userbank(){
	var shop="";
	var bank="";
	
	$.post(param,{route:{app:'LoadUserBank',key:key,UserNum:UserNum}},function(data){
		
		if(data=="1"){
			alert("system is having problem loading the information");
		}else{
			var x = JSON.parse(data);
			var storesize=x.store.length;
			var banksize =x.bank.key.length;
				storesize-=1;
			for(var a=0;a<storesize;a++){
				shop+="<tr data-typez='add' data-keyz='"+x.store[a].ProductNum+"'><td id='addz' class='UserN'><input type='checkbox' ></td><td class='UserN'>"+x.store[a].ProductNum+"</td><td class='UserN'>"+x.store[a].ItemMain+"</td><td>"+x.store[a].ItemSub+"</td><td>"+x.store[a].ItemName+"</td></tr>";
			}
			for(var a=0;a<banksize;a++){
				bank+="<tr data-typez='del' data-keyz='"+x.bank.key[a]+"'><td class='UserN'><input type='checkbox'></td><td class='UserN'>"+x.bank.key[a]+"</td><td class='UserN'>"+x.bank.ProductNum[a]+"</td><td>"+x.bank.PurPrice[a]+"</td><td>"+x.bank.ItemName[a]+"</td></tr>";
			}
		}
		var bank_element="<div class='shoplist-wrapper'><div class='item-shoplist lfloat'><table class='table'><thead><tr><td class='UserN'>#</td><td>Item #</td><td>Main</td><td>Sub</td><td>ItemName</td></tr></thead><tbody>"+shop+"</tbody></table></div><div class='side-menu rfloat'><p>Shop Item Map</p>Search Item<input type='text' class='input2'/><br><br><button onclick='insert_item_toBank()'>Insert to Bank</button></div></div><div class='shoplist-wrapper span2'><div class='item-bank lfloat'><table class='table'><thead><tr><td class='UserN'>#</td><td>Key #</td><td>Item #</td><td>Pur Price</td><td>ItemName</td></tr></thead><tbody>"+bank+"</tbody></table></div><div class='side-menu rfloat'><p>User Item Bank</p>User Num<input type='text' class='input2' value='143' disabled/><br><br>User Name<input type='text' class='input2' value='tearhear18' disabled/><button onclick='delete_item_toBank()'>Delete Item</button></div></div></div>";
		search_edit.empty().html(bank_element);
			$('table tr').click(function(event){
				
				if (event.target.type !== 'checkbox') {
				  $(':checkbox', this).trigger('click');
				}
				$("input[type='checkbox']").change(function (e) {
					if ($(this).is(":checked")) { //If the checkbox is checked
						$(this).closest('tr').addClass("active"); 
						//Add class on checkbox checked
					} else {
						$(this).closest('tr').removeClass("active");
						//Remove class on checkbox uncheck
					}
				});
			});
			 
	});
	
}
function insert_item_toBank(){
	myBasket=[];
	if(ProductNum==null||UserNum==null){ alert('unable to to insert item'); return false;}
	$('table tr').filter(':has(:checkbox:checked)').each(function() {
       
        $tr = $(this);
		var type=($(this).data('typez'));
		var key=($(this).data('keyz'));
        
		 if(type=='add'){
			myBasket.push(key);
		 }
		
    });
	
	$.post(param,{route:{app:'InsertitemtoBank',key:key,UserNum:UserNum,ProductNum:myBasket}},function(data){
			myBasket=[];
			load_sub_userbank();
	});
	
}
function delete_item_toBank(){
	myBasket2=[];
	if(ProductNum==null||UserNum==null){ alert('unable to to insert item'); return false;}
	$('table tr').filter(':has(:checkbox:checked)').each(function() {
       
        $tr = $(this);
		var type2=($(this).data('typez'));
		var key2=($(this).data('keyz'));
    
		 if(type2=='del'){
			myBasket2.push(key2);
		 }
		
    });
	
	
	$.post(param,{route:{app:'DeleteitemBank',key:key,UserNum:UserNum,ProductNum:myBasket2}},function(data){
		
			load_sub_userbank();
	});
	
}


function load_sub_userlocker(en){
	 if(typeof(en)==='undefined') en =1;
	$.post(param,{route:{app:'LoadUserLocker',key:key,UserNum:UserNum}},function(data){
		if(data=="1"){alert('locker not found');return false;}
		//console.log(data);
		_locker =JSON.parse(data);
		
		populate_location(en);
		 $("#tab_handler").live('change', function() { populate_location(this.value); });
		 $('div #Litem').live('click', function(){
			
			var imei=$(this).data('ids');
			var tab=$(this).data('tabs');
			var c_x=$(this).data('c_x');
			var c_y=$(this).data('c_y');
				if(imei=="NA"){
					$("#item_info_holder").empty();
					$(".locker_inventory").find('.uifocus').removeClass('uifocus');
					 $(this).addClass('uifocus');
					load_item_list_locker(c_x,c_y,tab);
				}else{
					$(".locker_inventory").find('.uifocus').removeClass('uifocus');
					load_item_info(imei,tab);
					
				}
		 });
	});
		
	
}
function populate_location(en){
	var element="";
		element+="<div class='locker_inventory lfloat'><div class='locker-tab'><div class='tab-radio lfloat'>1<input id='tab_handler' type='radio' name='tab' value='1'></div><div class='tab-radio lfloat'>2<input id='tab_handler' type='radio' name='tab' value='2'></div><div class='tab-radio lfloat'>3<input id='tab_handler' type='radio' name='tab' value='3'></div><div class='tab-radio lfloat'>4<input id='tab_handler' type='radio' name='tab' value='4'></div><div class='tab-radio lfloat'>P<input id='tab_handler' type='radio' name='tab' value='5'></div></div><div class='clearfix'></div>";
		for(var a=0;a<8;a++){
			//console.log(x.tab1.pos[a]);
			element+="<div id='x0y"+a+"' class='item-icon-holder lfloat'><div id='Litem'  data-tabs='"+en+"' data-ids='NA' data-c_x='0' data-c_y="+a+" class='item-image'></div></div>";
			element+="<div id='x1y"+a+"' class='item-icon-holder lfloat'><div id='Litem'  data-tabs='"+en+"' data-ids='NA' data-c_x='1' data-c_y="+a+" class='item-image'></div></div>";
			element+="<div id='x2y"+a+"' class='item-icon-holder lfloat'><div id='Litem'  data-tabs='"+en+"' data-ids='NA' data-c_x='2' data-c_y="+a+" class='item-image'></div></div>";
			element+="<div id='x3y"+a+"' class='item-icon-holder lfloat'><div id='Litem'  data-tabs='"+en+"' data-ids='NA' data-c_x='3' data-c_y="+a+" class='item-image'></div></div>";
			element+="<div id='x4y"+a+"' class='item-icon-holder lfloat'><div id='Litem'  data-tabs='"+en+"' data-ids='NA' data-c_x='4' data-c_y="+a+" class='item-image'></div></div>";
			element+="<div id='x5y"+a+"' class='item-icon-holder lfloat'><div id='Litem'  data-tabs='"+en+"' data-ids='NA' data-c_x='5' data-c_y="+a+" class='item-image'></div></div><div class='clearfix'></div>";
		}
	
		element+="<div class='gold-holder'>Gold: "+_locker.gold+"</div><div class='clearfix'></div></div><div id='item_info_holder' class='locker_value_holder lfloat'></div>";
		search_edit.empty().html(element);

	position="tab"+en;
	var size=_locker[position].pos.length;
	for(var i=0;i<size;i++){
		$("#"+_locker[position].pos[i]).html("<div id='Litem' data-ids='"+i+"' data-tabs='"+en+"' class='sprite-icon giftbox'></div>");
	}
}
function load_item_info(imei,tab){	
	
	position="tab"+tab;
	var info="<p>Item INFO </p><table><tbody><tr><td>Pos (X,Y)</td><td><input class='input1' type='text' value='"+_locker[position].X[imei]+"'disabled/> <input class='input1' type='text' value='"+_locker[position].Y[imei]+"'disabled/></td><td></td><td></td></tr><tr><td>ITEM MID SID</td><td><input class='input1' type='text'value='"+_locker[position].MID[imei]+"'disabled/> <input class='input1' type='text'value='"+_locker[position].SID[imei]+"'disabled/></td><td></td><td></td></tr><tr><td>Item Name</td><td><input type='text' value='"+_locker[position].ItemName[imei]+"'disabled/><td></td><td></td></td></tr><tr><td>Costume Name</td><td><input type='text'/><td></td><td></td></td></tr><tr><td>ITEM GRADE</td><td></td><td></td><td></td></tr><tr><td>Damage</td><td><input class='input1' type='text' value='"+_locker[position].DMG_GRADE[imei]+"'/></td><td></td><td></td></tr><tr><td>Defense</td><td><input class='input1' type='text' value='"+_locker[position].DEF_GRADE[imei]+"'/></td><td></td><td></td></tr><tr><td>Element</td><td>Fire <input class='input1' type='text' value='"+_locker[position].FIRE[imei]+"'/> Ice <input class='input1' type='text' value='"+_locker[position].ICE[imei]+"'/> Ele <input class='input1' type='text' value='"+_locker[position].ELEC[imei]+"'/></td><td></td><td></td></tr><tr><td></td><td>Poi&nbsp <input class='input1' type='text' value='"+_locker[position].POISON[imei]+"'/> Air&nbsp; <input class='input1' type='text' value='"+_locker[position].WIND[imei]+"'/></td><td></td><td></td></tr><tr><td>Quantity</td><td><input class='input1' type='text' value='"+_locker[position].QTY[imei]+"'/></td><td></td><td></td></tr><tr><td>Item Random Val</td><td><td></td><td></td></td></tr><tr><td>RV1</td><td><select id='rv1'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' type='text' value='"+_locker[position].Val1[imei]+"'/></td><td></td></td></tr><tr><td>RV2</td><td><select id='rv2'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' type='text' value='"+_locker[position].Val3[imei]+"'/></td><td></td></td></tr><tr><td>RV3</td><td><select id='rv3'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' type='text' value='"+_locker[position].Val2[imei]+"'/></td><td></td></td></tr><tr><td>RV4</td><td><select id='rv4'><option value='0'>NULL</option><option value='1'>DAMAGE(%)</option><option value='2'>DEFENSE(%)</option><option value='3'>HITRATE(+)</option><option value='4'>AVOIDRATE(+)</option><option value='5'>HP(+)</option><option value='6'>MP(+)</option><option value='7'>SP(+)</option><option value='8'>HP_INC(%)</option><option value='9'>MP_INC(%)</option><option value='10'>SP_INC(%)</option><option value='11'>HMS_INC(%)</option><option value='12'>GRIND_DAMAGE</option><option value='13'>GRIND_DEFENSE</option><option value='14'>ATTACK_RANGE</option><option value='15'>DIS_SP</option><option value='16'>RESIST</option><option value='17'>MoveSpeed</option><option value='18'>Damage(+)</option><option value='19'>Defense(+)</option><option value='20'>HitRate(%)</option><option value='21'>AvoidRate(%)</option><option value='22'>Pow(+)</option><option value='23'>Vit(+)</option><option value='24'>Int(+)</option><option value='25'>Dex(+)</option><option value='26'>Stm(+)</option><option value='27'>Melee(+)</option><option value='28'>Messile(+)</option><option avlue='29'>Energy(+)</option><option value='30'>HP Potion(+)</option><option value='31'>MP Potion(+)</option><option value='32'>SP Potion(+)</option><option value='33'>CP Gain(+)</option><option value='34'>CP(+)</option><option value='35'>Critical Damage(+)</option><option value='36'>Crushing Blow Damage(+)</option><option value='37'>Critical Rate(+)</option><option value='38'>Crushing Blow Rate(+)</option></select><input class='input1' type='text' value='"+_locker[position].Val4[imei]+"'/></td><td></td></td></tr><tr><td></td><td><button onclick='remove_locker_item("+imei+","+tab+")'>Remove Item</button></td><td></td><td></td></tr></tbody></table>";
	
	$("#item_info_holder").html(info);
	$("#rv1> option[value='"+_locker[position].Opt1[imei]+"']").attr("selected", "selected");
	$("#rv3> option[value='"+_locker[position].Opt2[imei]+"']").attr("selected", "selected");
	$("#rv2> option[value='"+_locker[position].Opt3[imei]+"']").attr("selected", "selected");
	$("#rv4> option[value='"+_locker[position].Opt4[imei]+"']").attr("selected", "selected");

	
}

