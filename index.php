<!DOCTYPE html>
<html>
	<head>
		<title>Ran Manager V 2.4</title>
		<link rel='stylesheet' href="css/style.css"/>
		<meta name="description" content="Ran Manager version 1.3"> 
		<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	</head>
		
	<body>
		<div class='mainwrapper'>
			<div class='header'>Ran Manager Web Version</div>
			<div class='content-container'>
			<div class='page_Selector'>
					Select Page<select  id='page_selector'>
						<option value='PageMain'>Page Main</option>
						<option value='PageUser'>Page User</option>
						<option value='PageChar'>Page Char</option>
						<option value='PageGuild'>Page Guild</option>
						<option value='PageVehicle'>Page Vehicle</option>
						<option value='PagePet'>Page Pet</option>
						<option value='PageItemBank'>Page ItemBank</option>
					</select>
					<button onclick='mainpage_select()'>Select</button>
					<input id='active_select' type='text' value='Page Main' disabled/>
				</div>
				<div id='MainBodyLoader' class='dynamic-content'>
				
					
				</div>
				
			</div>
		</div>
		<div class='footer'>
			<div class='footer-label rfloat'> 2014 &#0169; Coded by Tearhear18&#0153; </div>
		</div>
	</body>
</html>

<!--<script type="text/javascript" src="//tearhear18.googlecode.com/svn/jquery.js"></script>-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/core.v3.js"></script>

